# Readme
CORSS - Cylinder Optimization of Rings, Skin, and Stringers

Ported from NASA Technical Paper 3457 (1994)

# Introduction
This port of the CORSS program replaces the optimization library
-- formerly the commercial package DOT by Vanderplaats Research & Development, Inc -- 
with the open-source NLOpt library

# References
- NASA Technical Paper 3457, "CORSS: Cylinder Optimization of Rings, Skin, and Stringers," Marshall Space Flight Center, 1994.
- NASA Technical paper 3551, "CORSSTOL: Cylinder Optimization of Rins, Skin, and Stringers With Tolerance Sensitivity," Marshall Space Flight Center, 1995.