# TODO

Running list of open development tasks

## 09-26-2017
- Consolidate function definitions from remaining `*.h` files into `corss.h` header
- Compare header file definitions between CORSS and CORSSTOL papers. Map out `*.c` files and their functions
- Sort out intended difference (if any) between `ct.c` and `corsstol.c`
- Determine which `NLOPT` algorithms to use in lieu of `DOT` algorithms
- Write interfacing function(s) for `NLOPT` that use same syntax as function that calls `DOT`
- Ongoing typo fixes and code cleanup