#include "corsstol.h"

int iinput(FILE *infile)
{
    char hold[80];
    int  i  =  0;
    fscanf(infile," %d",&i);
    fgets(hold,80,infile);
    return(i);
}  /***************************************************** end   iinput ***/

float finput(FILE *infile)
{
    char hold[80];
    float f = 0;
    fscanf(infile," %f",&f);
    fgets(hold,80,infile);
    return(f);
} /****************************************************** end finput ****/

char cinput(FILE *infile)
{
    char hold[80];
    char c = '0';
    fscanf(infile," %1s",&c);
    fgets(hold,80,infile);
    return(c);
} /***************************************************** end cinput ****/

void readinput(char Title[], FILE *in, long int *IPRINT, long int *METHOD,
        int *SSP, struct material *M, struct cylinder *C, struct stringer *S,
        struct ring *R, struct load *L, float XL[], float X[], float XU[],
        float Tol[], int *mflag, char *LEB)
{
fgets(Title,80,in);

*IPRINT = iinput(in);
*METHOD = iinput(in);
*SSP = iinput(in);

M->nu = finput(in);
M->E = finput(in);
M->G = finput(in);
M->rho = finput(in);
M->Stu = finput(in);
M->Scy = finput(in);

C->r = finput(in);
C->l = finput(in);
C->fwt = finput(in);

S->stype = cinput(in);
*LEB = cinput(in);
*mflag = iinput(in);
S->alp = finput(in);
S->l1 = finput(in);
S->MZs = finput(in);

R->A = finput(in);
R->I = finput(in);
R->Z = finput(in);
R->J = finput(in);
R->N = finput(in);

L->F = finput(in);
L->M = finput(in);
L->V = finput(in);
L->Pa = finput(in);
L->Ph = finput(in);
L->sf = finput(in);
L->sfp = finput(in);

XL[0] = finput(in);
X[0] = finput(in);
XU[0] = finput(in);
Tol[0] = finput(in);

XL[1] = finput(in);
X[1] = finput(in);
XU[1] = finput(in);
Tol[1] = finput(in);

XL[2] = finput(in);
X[2] = finput(in);
XU[2] = finput(in);
Tol[2] = finput(in);

XL[3] = finput(in);
X[3] = finput(in);
XU[3] = finput(in);
Tol[3] = finput(in);

XL[4] = finput(in);
X[4] = finput(in);
XU[4] = finput(in);
Tol[4] = finput(in);

XL[5] = finput(in);
X[5] = finput(in);
XU[5] = finput(in);
Tol[5] = finput(in);

} /****************************************** end readinput ****/


void pinput(struct stringer S, struct ring R, struct material M,
        struct load L, struct cylinder C, long int IPRINT, long int METHOD,
        FILE *out, int SSP, float X[], float XL[], float XU[], int mflag,
        char LEB)
{
fprintf(out,"\n\n************************ INPUT VALUES *****************");
fprintf(out,"\nFLAGS");
fprintf(out,"\n    IPRINT = %d",IPRINT);
switch ( (int)IPRINT)
{
case 0:
    fprintf(out,"    No screen output by DOT");
    break;
case 1:
    fprintf(out,"    initial and final DOT output to screen");
    break;
case 2:
    fprintf(out,"    initial/final + OBJ & X[] output to screen");
    break;
case 3:
    fprintf(out,"    initial/final + OBJ & X[] + G[] output to screen");
    break;
case 4:
    fprintf(out,"    init./final + OBJ & X[] + G[] + grads. to screen");
    break;
case 5:
    fprintf(out,"    init./final + OBJ & X[] + G[] + grads. to screen");
    break;
case 6:
    fprintf(out,"    init./final+OBJ & X[]+G[]+grads.+S+X scales & S info");
    break;
case 7:
    fprintf(out,"    init./final+OBJ & X[]+G[]+grads.+S+X scales & S info");
    break;
}
fprintf(out,"\n    METHOD = %d",METHOD);
// TODO update for methods used by NLOpt
if (2 == METHOD){
    fprintf(out,"    Sequential Linear Programming");
}
else{
    fprintf(out,"    Modified Method of Feasible Directions");
}
fprintf(out,"\n    SSP = %d   ",SSP);

switch (SSP)
{
case 0:
    fprintf(out,"    Final CORSS output only");
    break;
case 1:
    fprintf(out,"    Final calculations and final output");
    break;
case 2:
    fprintf(out,"    Optimization history and final output");
    break;
case 3:
    fprintf(out,"    Optimization history, final calculations and");
    fprintf(out," final output");
}

fprintf(out,"\n\nMaterial Properties");
fprintf(out,"\n    nu     =  %-6.3f              Poisson's Ratio",M.nu);
fprintf(out,"\n    E      =  %-11.3E         Young's Modulus",M.E);
fprintf(out,"\n    SM     =  %-11.3E         Shear Modulus",M.G);
fprintf(out,"\n    rho    =  %-6.3f              Density",M.rho);
fprintf(out,"\n    Stu    =  %-8.1f            Ultimate Tensile Stress",M.Stu);
fprintf(out,"\n    Scy    =  %-8.1f            Yield Compressive Stress",M.Scy);

fprintf(out,"\n\nCylinder Geometry");
fprintf(out,"\n    r    =  %-7.2f               Radius",C.r);
fprintf(out,"\n    l    =  %-7.2f               Length",C.l);
fprintf(out,"\n    fwt  =  %-7.2f               Additional Weight",C.fwt);

fprintf(out,"\n\nStringer Parameters");
fprintf(out,"\n    stype   =  %c                         ",S.stype);

if (S.stype == 'H'){
    fprintf(out,"Hat Stringers\n    Local Elastic Buckling");
    if ('N'==LEB){
        fprintf(out," NOT ALLOWED");
    }
    else{
        fprintf(out," IS ALLOWED");
    }
    fprintf(out,"\n    alp  = %-5.1f      Leg angle",S.alp);
    fprintf(out,"\n    l1   = %-6.3f      Stringer/skin length",S.l1);
}
else{
    fprintf(out, "I Stringers\n    Local Elastic Buckling");
    if ('N' == LEB){
        fprintf(out," NOT ALLOWED");
    }
    else{
        fprintf(out," IS ALLOWED");
    }
    fprintf(out,"\n    increase m from 1");
    if(0 == mflag){
        fprintf(out," while coupled buckling stress decreases");
    }
    else{
        fprintf(out," to %d",mflag);
    }
    fprintf(out,"\n   alp   = %-5.3f        Web Angle",S.alp);
    fprintf(out,"\n   l1    = %-4.3f        Bottom flange thickness",S.l1);
}
fprintf(out,"\n    MZs   = %-2.0f           Stringers ",S.MZs);
if (-1 == S.MZs){
    fprintf(out,"internal");
}
else{
    fprintf(out,"external");
}

fprintf(out,"\n\nRing Parameters");
if (0 == R.N){
    fprintf(out,"\n    No Rings");
}
else{
    fprintf(out,"\n    Ar    = %-7.4f       Cross sectional area",R.A);
    fprintf(out,"\n    Ir    = %-8.5f      Moment of inertia",R.I);
    fprintf(out,"\n    Zr    = %-7.4f       Neutral Axis Distance",R.Z);
    fprintf(out,"\n    Nrng  = %-2.0f            Number of Rings",R.N);
}

fprintf(out,"\n\nLoads");
fprintf(out,"\n     F  = %-9.1f          Axial Compression",L.F);
fprintf(out,"\n     M  = %-11.4G        Bending Moment",L.M);
fprintf(out,"\n     V  = %-8.1f           Shear force",L.V);
fprintf(out,"\n     Pa  = %-6.3f           Axial pressure component",L.Pa);
fprintf(out,"\n     Ph  = %-6.3f           Hoop pressure component",L.Ph);
fprintf(out,"\n     sf  = %-5.2f            Safety factor",L.sf);
fprintf(out,"\n     sfp = %-5.2f            Plate buckling safety factor",L.sfp);

fprintf(out,"\n\nDesign Variables");
fprintf(out,"\nVar.      Minimum      Initial      Maximum      Name");
fprintf(out,"\nh  %14.4f  %12.4f  %12.4f      Stringer Height",XL[0],X[0],XU[0]);
fprintf(out,"\nl2 %13.4f  %12.4f  %12.4f      Top flange ",XL[1],X[1],XU[1]);

if ('H' == S.stype){
    fprintf(out,"length");
}
else{
    fprintf(out,"thickness");
}
fprintf(out,"\ntst  %12.4f  %12.4f  %12.4f ",XL[2],X[2],XU[2]);
if ('H' == S.stype){
    fprintf(out,"Stringer thickness");
}
else{
    fprintf(out,"Web thickness");
}
fprintf(out,"\nNst %12.1f  %12.1f  %12.1f      Number of stringers",XL[3],X[3],XU[3]);
fprintf(out,"\nt   %12.4f  %12.4f  %12.4f      Skin thickness",XL[4],X[4],XU[4]);
fprintf(out,"\nW   %12.4f  %12.4f  %12.4f      ",XL[5],X[5],XU[5]);
if ('H' == S.stype){
    fprintf(out,"Top Flange Thickness");
}
else{
    fprintf(out,"Stringer Width");
}
fprintf(out,"\n\n");
fflush(out);
}/**************************** end pinput ****/


void finalout(struct ring R, struct stringer S, struct material M,
            struct load L, struct cylinder C, float G[], float gamF, float gamM,
            char GCB, int mgcb, float N, int ncr, int ngcb, float Nx, float obj,
            FILE *out, float Pcrush, float Scol, float Scrip1, float Scrip2,
            float Scrstcol, char LEB, int mdrv, float Io, float Faxial)
{
putchar('F');
fprintf(out,"\n\n------------------------------------------------------------" );
fprintf(out,"--------------------");
fprintf(out,"\n\nCylinder Weight = %3.1f\n (Skin:",obj);
fprintf(out," %3.1f, Stringers: %3.1f", (pi*2*C.r*C.t*C.l*M.rho), (S.A*C.l*S.N*M.rho));
fprintf(out,", Rings: %3.1f, Flanges: %3.1f)", (R.A*R.N*2*pi*(C.r+R.Z)*M.rho),C.fwt);
fprintf(out,"\n\nh   = %6.4f,     Stringer Height",S.h);
fprintf(out,"\nl2  = %6.4f,     ",S.l2);
if (S.stype == 'H') fprintf(out,"Hat stringer top flange length");
if (S.stype == 'I') fprintf(out,"I top flange thickness");
fprintf(out,"\ntst = %6.4f,     ",S.t);
if (S.stype == 'H') fprintf(out,"Hat stringer thickness");
if (S.stype == 'I') fprintf(out,"I stringer web thickness");
fprintf(out,"\nNst = %6.1f,     Number of Stringers, b = %6.4f",S.N,S.b);
fprintf(out,"\nt   = %6.4f,     Skin Thickness",C.t);
fprintf(out,"\nW   = %6.4f,     ",S.W);
if ('H' == S.stype) fprintf(out,"Hat stringer top flange thickness");
if ('I' == S.stype) fprintf(out,"I stringer width");
fprintf(out, "\n\nStringer: I = %f, J = %f, Z = %f, A = %f",S.I,S.J,S.Z,S.A);
fprintf(out, "\nEnd Ring I should be at least ");
fprintf(out, "%g", .172*Scrstcol*(S.A+S.b*C.t)*C.r*C.r*C.r/R.d/M.E );
fprintf(out, "\nEnd Ring Area should be at least");
fprintf(out, " %g*(r+Z)",4*pi*pi*S.I*C.r/S.b/pow(R.d,3));
fprintf(out, "\n\t\t\t\t\t\t\t\t  G[] value");
fprintf(out, "\nSkin: (Shear ratio)**2 + Stress ratio\t\t = %7.5f < 1 %11.5f",G[0]+1,G[0]);
fprintf(out, "\n\nApplied Column Stress (SF = %4.2f)                = %11.1f",L.sf,Scol);

if ('I'==S.stype){
    fprintf(out,"\n    Critical Coupled Buckling Stress (m = %3d)   = %8.1f",mdrv, Scrip2);
    fprintf(out,"%15.5f",G[4]);
    if ('N'==LEB){
        fprintf(out,"\n    Critical Flange Elastic Buckling Stress    = %8.1f",Scrip1);
    }
    else{
        fprintf(out,"\n    Critical Stringer Crippling Stress       = %8.1f",Scrip1);
    }
    fprintf(out,"%15.5f",G[3]);
}
else{
    if ( 'N' ==LEB){
        fprintf(out,"\n    Critical Local Buckling Stress (flange)    = %8.1f",Scrip1);
        fprintf(out,"%15.5f",G[3]);
        fprintf(out,"\n    Critical Local Buckling Stress (web)       = %8.1f",Scrip2);
        fprintf(out,"%15.5f",G[4]);
    }
    else{
        fprintf(out,"\n    Critical Stringer Crippling Stress         = %8.1f",Scrip1);
        fprintf(out,"%15.5f",G[3]);
    }
}
fprintf(out,"\n    Critical Column Stress ");
if ('N' == GCB){
    fprintf(out,"            = %8.1f",Scrstcol);
    fprintf(out,"%15.5f",G[1]);
}
else fprintf(out,"controlled by General Cylinder Buckling");
fprintf(out,"\n\nApplied Von Mises Stress (SF = %4.2f)           = %11.1f",L.sf, (G[2]+1)*M.Scy);
fprintf(out,"\n    Yield Compressive Stress                   = %8.1f",M.Scy);
fprintf(out,"%15.5f",G[2]);
if ('N' == GCB){
    fprintf(out,"\n\nGeneral Cylinder Buckling controlled by ");
    fprintf(out,"Critical Column Buckling");
}
else{
    fprintf(out,"\n\nCylinder: Line Load ratio + Pressure ratio");
    fprintf(out,"      = %7.5f < 1 %11.5f",G[1]+1,G[1]);
    fprintf(out,"\nApplied Cylinder Line Load (SF = %4.2f)           = %11.1f",L.sf, (L.M*L.sf*C.r/Io*(S.A/S.b+C.t) + Faxial*(S.A/S.b+C.t)));
    fprintf(out,"\nKnockdown Adjusted Line Load                     = %11.1f",N);
    fprintf(out,"\n    Critical General Cylinder Buckling Line Load = %8.1f",Nx);
    fprintf(out,"\n    Axial half waves, m = %i, Hoop waves,  n = %i",mgcb, ngcb);
    fprintf(out,"\n    Axial   Knockdown Factor, gammaF = %6.4f",gamF);
    fprintf(out,"\n    Bending Knockdown Factor, gammaM = %6.4f",gamM);
    if ( (mgcb != 1) && (mgcb == R.N+1) ){
        fprintf(out,"\nAxial half waves = Number of Cylinder segments");
        fprintf(out,"\n    Rings may not support general buckling");
    }
}
if (L. Ph<0.0){
    fprintf(out,"\nApplied Crushing Hoop Pressure (SF = %4.2f)      = %16.3f",L.sf,-L.Ph*L.sf);
    fprintf(out,"\n    Critical Buckling Pressure                  = %8.3f",Pcrush);
    fprintf(out,"\n    Hoop waves, n = %i",ncr);
}
if ((Nx*(S.A/S.b+C.t))>M.Scy){
    fprintf(out,"\n    If load increases over limit load, critical");
    fprintf(out,"\n      buckling will decrease due to plasticity.");
}
} /******************************** end finalout ***/
