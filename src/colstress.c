#include "corsstol.h"

float colstress(float *Anew, struct stringer S, float d, float E,
                struct load L, struct cylinder C, float Faxial, float Io, FILE *out,
                float *St, float *Scol, float Scrip, float Scrpl, float *Scrstcol,
                float Sskbd, int SSP, float taucr, float tauskbd, float Ysk[],
                float Yst[])
{
float be, Ise,radg,ScrstcolJE,We0,tmp;
putchar('C');
*Scol = L.M*L.sf*C.r/Io + Faxial;
be = S.b;
if (L.sfp<L.sf){
    if (L.sf/L.sfp*Sskbd/Scrpl+pow(L.sf/L.sfp*tauskbd/taucr,2) > 1){
        *Scol = loopI(Anew, S,L.M,L.sf,E,C,Faxial,Io,out,St,Scrpl,SSP,&We0,Ysk,Yst);
        if (S.stype == 'H'){
            if ( (tmp=S.l1+S.l2+2*S.la) < 2*We0 ){
                be=tmp; /* between legs */
            }
            else{
                be=2*We0;
            }
            if ( (tmp=S.b-S.l1-2*S.la-S.l2) < 2*We0 ){
                be+=tmp;
            }
            else{
                be+=2*We0;
            }
        }
        else if ( S.b > 2*We0 ){
            be=2*We0;
        }
    }
}
Ise = be*C.t*C.t*C.t/12. + S.I + S.A*S.Z*S.Z - S.A*S.A*S.Z*S.Z/(S.A+be*C.t);
if (Ise<0){
    Ise = 0.00001;
}
radg = sqrt(Ise/(S.A+be*C.t));
if (d/radg <= pi*sqrt(2*E/Scrip)){
    *Scrstcol = Scrip - Scrip*Scrip/4/pi/pi/E*d*d/radg/radg;
    if (SSP == 4){
        fprintf(out,"\n\nJohnson-Euler Column Buckling (Scrip = %g) = %g",Scrip,*Scrstcol);
    }

}
else{
    *Scrstcol = pow(pi*radg/d,2)*E;
    if (SSP == 4){
        fprintf(out,"\n\nEuler Column Buckling = %g",*Scrstcol);
    }
}
if (SSP == 4){
    fprintf(out,"\nApplied Stress = %8.1f",*Scol);
    fprintf(out,"\nEffective skin width on stringer with max stress = %f",be);
    fprintf(out,"\nStringer+Skin I = %6.4f, radius of gyration = %6.4f",Ise,radg);
}
return(*Scol/(*Scrstcol) -1. );
} /************************ end colstress ****/

float loopI(float *Anew, struct stringer S, float M, float sf, float E,
            struct cylinder C, float Faxial, float Io, FILE *out, float *St,
            float Scrpl, int SSP, float *We0, float Ysk[], float Yst[])
{
float Acyl, I, Iold, ybar;
int count;
count = 0;
ybar = 0;
I = Io/2;
Iold = Io;
Acyl = 2*pi*(C.r-C.t/2)*C.t+S.N*S.A;
*Anew = Acyl;
while( fabs((Iold-I)/Iold) > .001 ){
    Iold = I;
    count++;
    newI(Acyl,Anew, S,M, sf,C.t,E,Faxial,&I,Io,out,Scrpl,SSP,We0,&ybar,Ysk,Yst);
    if (count == 20){
        fprintf(out,"\nDid not converge on a Cylinder I in 20 iterations,");
        fprintf(out," I = %2.0f, I set to 1/2 Io =",I);
        I = Io/2;
        fprintf(out," %2.0f, t = %6.4f, Nst = %5.1f",I,C.t,S.N);
        break;
    }
}
*St = -M*sf*(C.r-ybar)/I + Faxial*Acyl/(*Anew);
if (SSP == 4){
    fprintf(out,"\n\nNumber of iterations to converge on I, %d",count);
    fprintf(out,"\n\nTension load (adjusted for buckled skin) = %8.1f",*St);
    newI(Acyl,Anew, S,M, sf,C.t,E, Faxial,&I,Io,out,Scrpl,6,We0,&ybar,Ysk,Yst);
}
return( M*sf*(C.r+ybar)/I + Faxial*Acyl/(*Anew) );
} /********** end loopI ****/


void newI(float Acyl, float *Anew, struct stringer S, float M, float sf,
        float t, float E, float Faxial, float *I, float Io, FILE *out,
        float Scrpl, int SSP, float *We0, float *ybar, float Ysk[],
        float Yst[])
{
float A, Slb, Ssk[541+1],Sst[541+1],sumA, sumAy,sumAyy,We[541+1],tmp;
int i,N;
N = S.N/2;
Slb = .9*Scrpl;
if (SSP == 6){
    fprintf(out,"\n\nStress Carried in Buckled Skin, Slb = %7.1f",Slb);
    fprintf(out,"\n\n  I    Y-Stringer        Y-skin   Stress-str.");
    fprintf(out,"   Stress-skin    Eff. Width");
}
for (i=0; i<N/2+1; i++){
    Sst[i] = Faxial*Acyl/(*Anew) + M*sf/(*I)*(Yst[i]+(*ybar));
    Ssk[i] = Faxial*Acyl/(*Anew) + M*sf/(*I)*(Ysk[i]+(*ybar));
    if (Sst[i] < 0){
        We[i] = S.b;
    }
    else if ( (We[i] = .85*t*sqrt(E/Sst[i])) > S.b ){
        We[i] = S.b;
    }
    if (SSP == 6){
        fprintf(out,"\n%3i %13.4f %13.4f %13.1f %13.1f %13.4f",i,Yst[i],Ysk[i],Sst[i],Ssk[i],We[i]);
    }
}
A= 0;
sumA = 0;
sumAy = 0;
sumAyy = 0;
We[N] = We[0];
if (SSP == 6){
    fprintf(out,"\n\n   I           Area        sum Area        sum Area*y");
    fprintf(out,"    sum Area*y*y skin/str.");
}
for (i=0; i<(N/2); i++){
    if ( (S.stype == 'H') && (Sst[i] > 0) ){
        if ( (tmp = S.l2+2*S.la+S.l1) > (2*We[i]) ){
            A = 2*( t*(Sst[i] - Slb)/Sst[i]*(tmp-2*We[i]) );
            sumA += A;
            sumAy += A*Yst[i];
            sumAyy += A*Yst[i]*Yst[i];
            if (SSP == 6){
                fprintf(out,"\n%3i %15.3f %15.3f %15.3f %15.3f stringer",i,A, sumA, sumAy,sumAyy);
            }
        }
    }
    if (Ssk[i] > 0){
        if (S.stype == 'H'){
            if ( (tmp=S.b-(S.l2+2*S.la+S.l1)) > (We[i] + We[i+1]) ){
                A = 2*( t*(Ssk[i] - Slb)/Ssk[i]*(tmp-We[i]-We[i+1]) );
                sumA += A;
                sumAy += A*Ysk[i];
                sumAyy += A*Ysk[i]*Ysk[i];
                if (SSP == 6){
                    fprintf(out,"\n%3i %15.3f %15.3f %15.3f %15.3f skin",i, A, sumA, sumAy, sumAyy) ;
                }
            }
        }
        else if ( S.b > (We[i]+We[i+1]) ){
            A = 2*( t*(Ssk[i] - Slb)/Ssk[i]*(S.b-We[i]-We[i+1]) ) ;
            sumA += A;
            sumAy += A*Ysk[i];
            sumAyy += A*Ysk[i]*Ysk[i];
            if (SSP == 6){
                fprintf(out,"\n%3i %15.3f %15.3f %15.3f %15.3f skin",i, A, sumA, sumAy, sumAyy) ;
            }
        }
    }
}
*We0 = We[0];
*Anew = Acyl-sumA;
*ybar = sumAy/(*Anew);
*I = Io - sumAyy - (*Anew)*(*ybar)*(*ybar);
if (SSP==6){
    fprintf(out,"\n\n Ybar = %7.3f,    Cylinder I = %9.1f,    Anew = %9.1f",*ybar, *I, *Anew);
}
} /************************ end newI ****/
