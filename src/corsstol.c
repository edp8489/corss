#include "corsstol.h"


void corsstol(struct stringer S, struct ring R, struct material M,
        struct load L, struct cylinder C, int hap, long int IPRINT,
        int l2ap, long int METHOD, long int NCON, long int NDV, int Nrng,
        int Nstap, FILE *out, int SSP, int tap, float Tol[], int tstap,
        int Wap, float X[], float XL[], float XU[],int mflag, char LEB)
{
long int INFO,IPRM[20],IWK[110],j,MINMAX,NRIWK,NRWK;
float RPRM[20],WK[450];
float Gslope[5][6], GW[5], Wslope[6], Xnorm[12], AddTol[5][6], delWt[6]
    DVmin[6], DVmax[6], OBJ, OPTWT, NSTmin;
int i;
SSP = 0;
for (j=0; j<20; j++)
{
    RPRM[j] = 0.0;
    IPRM[j] =0;
}
NRWK = 450;   /* make the dimension of IWK and WK the same */
NRIWK = 110;
MINMAX = -1;
INFO = 0;
CTPinput(S.stype,Tol,out);
for (i=0; i<NDV; i++)
{
    Xnorm[i] = X[i];
    XL[i] = XL[i]/Xnorm[i];
    XU[i] = XU[i]/Xnorm[i];
    X[i] = 1.;
}
for (i=NDV; i<12; i++)
{
    Xnorm[i] = 1.;
}
if (NDV==0){
    OBJ = EVALTOL(S,R,M,L,C,Gslope,GW,hap,l2ap,Nstap,out,tap,Tol,tstap,
        Wap,X,Xnorm,SSP,mflag,LEB,NCON);
}
else
{
INFO = 0;
do
{
    OBJ = EVALTOL(S,R,M,L,C,Gslope,GW,hap,l2ap,Nstap,out,tap,Tol,tstap,
        Wap,X,Xnorm,SSP,mflag,LEB,NCON);
    DOT(&INFO,&METHOD,&IPRINT,&NDV,&NCON,X,XL,XU,&OBJ,&MINMAX,GW,RPRM,IPRM,
        WK,&NRWK,IWK,&NRIWK);
} while (INFO!=0);
}
printf("\nToleranced Optimization Complete, Max Weight = %f",OBJ);
for (i=0; i<NDV; i++} X[i] *= Xnorm[i];
NSTmin = 2*pi*C.r/(2*pi*C.r/X[Nstap]+Tol[3]);
for (i=0; i<NCON; i++) GW[i] -= 1.E-5;
TOLOUT(AddTol,S.alp,R.A,delWt,DVmax,DVmin,C.fwt,C.l,Gslope,GW,S.ll,
    R.N,&OPTWT, C.r,M.rho,S.stype,Tol,Wslope,X,R.Z,hap,l2ap, tstap,
    Nstap,tap,Wap,NSTmin,NCON);
CTfinalout(OPTWT,OBJ,Wslope,delWt,Gslope,GW,AddTol,DVmin,DVmax,S.stype,
    out,Tol,C.r,X[hap],X[l2ap],X[tstap],2*pi*C.r/X[Nstap],
    X[tap],X[Wap],X[Nstap],NSTmin,LEB,NCON) ;

} /********** end CORSSTOL ********/


float WEIGHT(float A, float C, float D, float EE, float FF,
             float FWT, float L, float L1, float NRNG, float P,
             float R, float RHO, float stype, float ALP, float AR, float ZR)
{
float ALPHA, HA, AS, RASL, RISL;
ALPHA = (ALP*pi)/180.0;
if ('H'==stype)
{
    HA = (A-D-P)/(cos(ALPHA));
    AS = (2.0*(L1*D)) + (2.0*(HA*D)) + (C*P);
}
else
{
    HA = (A-C-L1)/(cos(ALPHA));
    AS = (P*L1) + (D*HA) + (P*C);
}
RASL = R-(FF/2.0);
RISL = R-FF;
return (((pi*2.0*RASL*FF*L) + (AS*L*EE) + ((AR*NRNG*2.0*pi) * (RISL+ZR)))*RHO + FWT);
} /******** end WEIGHT ********/
