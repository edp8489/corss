/** C language include files for standard function definitions */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
/** define pi for use in the program */
#define pi 3.141592654

/** Variable Structures
Many often passed values are grouped into structures of related variables
stiffness - the smeared orthotropic cylinder properties used in General
    Cylinder Buckling Calculations
    Ex, Ey, Exy - smeared extensional stiffnesses
    Gxy - smeared shear stiffness
    Cx, Cy, Cxy, Kxy - smeared coupling constants
    Dx, Dy, Dxy - smeared bending stiffnesses
material - the material properties from the input file
    E - Young's Modulus
    nu - poisson's ratio
    rho - density
    Scy - compressive yield strength
    G - Shear Modulus
    Stu - tensile ultimate strength
load - the loading parameters from the input file
    F - axial force, positive is compressive
    M - bending moment
    Pa - axial pressure (i.e. ullage), internal is positive
    Ph - hoop pressure (i.e. ullage+head), internal is positive
    sf - safety factor to be applied to loads
    sfp - skin buckling safety factor, assumed 1.0<=sfp<=sf
    V - shear force
stringer - the variables that define the stringers
    A - cross sectional area
    I - Moment of Inertia about the neutral axis parallel to the skin
    J - The polar moment of inertia, Ix+Iy
    N - the number of stringers (optimized design variable)
    Z - neutral axis distance from the skin
    alp - web angle
    l1 - hats: stringer to skin length, 2/hat; I: bottom flange thickness
    MZs - Z multiplier, 1 for external stringers, -i for internal
    h - stringer height (optimized design variable)
    l2 - hat: top flange width, I: top flange thickness (design variable)
    t - skin thickness (optimized design variable)
    W - hat: top flange thickness, I: stringer width (design variable)
    b - stringer spacing
    la - projected length of the hat stringer web on the skin
    stype - stringer type identifier, H for hats, I for I's
ring - ring parameters from the input file
    A - cross sectional area
    I - moment of inertia about the neutral axis parallel to the skin
    J - Polar Moment of Inertia, Ix+Iy
    N - number of intermediate rings (not end rings)
    Z - neutral axis distance from the skin, + for external
    d - ring spacing
cylinder - cylinder geometry definitions
    fwt - additional weight, not optimized
    l - length of cylinder
    r - radius of cylinder
    t - skin thickness (optimized design variable)
*****************************************************************************/
struct stiffness{float Ex, Ey, Exy, Gxy, Dx, Dy, Dxy, Cx, Cy, Cxy, Kxy; };
struct material {float E, nu, rho, Scy, G, Stu; };
struct load     {float F, M, Pa, Ph, sf, sfp, V; };
struct stringer {float A, I, J, N, Z, alp, l1, MZs, h, l2, t, W, b, la;
               char stype;);
struct ring     {float A, I, J, N, Z, d; };
struct cylinder {float fwt, l, r, t; };

/****************************************************************************/
/******************************    file CT.C    *****************************/
/****************************************************************************/

/** MAIN starts the program, initialized the variables and calls the
    optimization routine
called from: DOS command line
calls : readinput, pinput, initDVs, CALLeval, and corsstol
returns :
    argc, argv - the number and array of command line parameters,
    G - array of constraint values,
    hap - position of stringer height in X
    i - index,
    in, out - handles for the input and output files,
    IPRINT - DOT output flag,
    l2ap - position of top flange length in X
    LEB - Y/N flag to allow local elastic buckling of the stringer segments
    METHOD - DOT method flag,
    mflag - flag to control search of I-coupled buckling
    NCON - number of constraints,
    NDV - number of design variables,
    Nstap - position of number of stringers in X
    OBJ - weight,
    SSP - CORSS output flag
    tap - position of skin thickness in X
    Title - title of run,
    Tol - array of tolerances associated with X
    tstap - position of stringer thickness in X
    Wap - position of I width: H top flange thickness in X
    X, XL, XU - array of design variables, and lower and upper limits,
    Xinit - array of initial variables for normalizing
*******************************************************************/

/** comlineerr is called if there is an error with the command line. It
    prints the correct usage message and exits the program
called from: MAIN
calls :
returns :
*******************************************************************/
void comlineerr();

/** initDVs controls the removal of variables from the design variable arrays
called from: MAIN
calls : Xadust
returns : modified X,XL,XU,Xinit arrays, and hap, l2ap, tstap,Nstap,tap,Wap
        indices and the new number of design variables
    i - incrementing index
    NDV - number of design variables
    pos - current X array position
**********************************************************************/

long int initDVs(float XL[], float X[], float XU[], int *hap, int *l2ap,
    int *tstap, int *Nstap, int *tap, int *Wap, float Xinit[],
    long int ndv);

/** Xadjust manipulates the design variable arrays so that only the needed
design variables are iterated upon
called from: initDVs
calls :
returns : next available design variable array position
    k - index
**********************************************************************/
int Xadjust(int newpos, int pos, long int *NDV, float X[], float Xinit[],
    float XL[], float XU[]);


/****************************************************************************/
/***************************    file CTOPT.C    *****************************/
/****************************************************************************/

// TODO
/***** THIS WILL NEED TO BE CHANGED TO UTILIZE THE C NLopt OPTIMIZATION LIBRARIES
******/

/** DOT is the numerical optimizer program
called from: ctopt
returns : new values for X
**************************************************************/
void DOT(long int *INFO, long int *METHOD, long int *IPRINT,
                long int *NDV, long int *NCON, float X[],
                float XL[], float XU[], float *OBJ,
                long int *MINMAX, float G[], float RPRM[],
                long int IPRM[], float WK[], long int *NRWK,
                long int IWK[], long int *NRIWK);


/** CALLeval uses the X array and the 'ap' variable to prepare the variables
for the analysis of EVAL
calls : eval
returns :
***************************************************************************/
void CALLeval(struct stringer S, struct ring R, struct material M,
        struct load L, struct cylinder C, float G[], float X[],
        float Xinit[], float *OBJ, FILE *out, int SSP, int hap, int l2ap,
        int Nstap, int tap, int tstap, int Wap, int mflag, char LEB);


/** ctopt controls the optimization calls to eval and DOT
called from: MAIN
calls : CALLeval and DOT
returns
INFO - DOT completion flag
IPRM, IWK, RPRM, WK - DOT working arrays
MINMAX - DOT minimization/maximization flag
NRIWK, NRWK - IWK and WK sizes
***************************************************************************/
void ctopt(int SSP, FILE *out, float X[], float Xinit[], int hap,
        int l2ap, int tstap, int Nstap, int tap, int Wap, struct stringer S,
        struct ring R, struct material M, struct load L, struct cylinder C,
        float G[], float *OBJ, long int *METHOD, long int *IPRINT,
        long int *NDV, long int *NCON, float XL[], float XU[], int mflag,
        char LEB);


/****************************************************************************/
/****************************    file eval.C    *****************************/
/****************************************************************************/

/** EVAL is the function that controls the analysis
called from: CALLeval
calls : stringer, skinbuck, getScrip, colstress, GCBcalc, stresscheck,
        and finalout
returns : G array values
    Anew - cylinder area with buckled skin,
    Faxial, Faxialp - axial stress from F and Pa only, w/ sf and sfp
    gamF - general cylinder buckling knockdown factor for axial loads
    gamM - general cylinder buckling knockdown factor for bending
    GCB - flag for identifying the shell buckling failure mode
    i - index
    Io - cylinder total moment of inertia
    mdrv - the critical wavelength value for I-coupled buckling
    mgcb, ngcb - half axial and hoop waves for Nx
    N - knockdown adjusted applied line load
    Nx - critical general cylinder buckling line loads
    ncr - hoop waves for Pcr
    Pcr- critical general cylinder buckling pressure
    rasl, risl - radius average skin line, inner skin line
    St - applied tension stress
    Scol, Scrstcol - applied and critical stringer/column stress
    Scrip1/2, - crippling and local buckling critical stresses
    ScripS - weighted average of inelastic crippling stress
    Scrpl, Sskbd - skin stresses: crit. skin buckling, skin buck. driver
    Ssk - maximum stress in the skin
    tau, taucr, tauskbd, tau0 - shear stresses: maximum, skin buckling
            critical,skin buckling driver, minimum
    theta, dtheta - angle and angle between stringers
    Yst, Ysk - stringer and skin y distances from cylinder center
*******************************************************************/
void eval(struct stringer S, struct ring R, struct material M, struct load L,
    struct cylinder C, float G[], float *OBJ, FILE *out, int SSP,
    int mflag, char LEB);


/****************************************************************************/
/*****************************    file CIO.C    *****************************/
/****************************************************************************/

/** finalout prints the last set of output data
called from: eval
calls :
returns :
*************************************************************************/
void finalout(struct ring R, struct stringer S, struct material M,
    struct load L, struct cylinder C, float G[], float gamF, float gamM,
    char GCB, int mgcb, float N, int ncr, int ngcb, float Nx, float obj,
    FILE *out, float Pcrush, float Scol, float Scrip1, float Scrip2,
    float Scrstcol, char LEB, int mdrv, float Io, float Faxial);



/** cinput, finput, iinput read a character, a float and an integer,
respectively from the input file then skips to the next line
called from: readinput
calls :
returns : a character, float, or integer
**********************************************************************/
char  cinput(FILE *infile);
float finput(FILE *infile);
int   iinput(FILE *infile);


/** pinput, repeats the information from the input file so that input and
output are always kept together
called from: main
calls :
returns :
**********************************************************************/
void pinput(struct stringer S, struct ring R, struct material M,
        struct load L, struct cylinder C, long int IPRINT, long int METHOD,
        FILE *out, int SSP, float X[], float XL[], float XU[], int mflag,
        char LEB);


/** readinput, reads the information in the input file
called from: main
calls
returns : all input information
*******************************************************************/
void readinput(char Title[], FILE *in, long int *IPRINT, long int *METHOD,
    int *SSP, struct material *M, struct cylinder *C, struct stringer *S,
    struct ring *R, struct load *L, float XL[], float X[], float XU[],
    float Tol[], int *mflag, char *LEB);



/****************************************************************************/
/**************************    file CORSSTOL.C    ***************************/
/****************************************************************************/

/** CALLevalCT arranges the CORSSTOL information and calls the analysis
called from: EVALTOL
calls : eval
returns : array of constraints for DOT
****************************************************************************/
void CALLevalCT(struct stringer S, struct ring R, struct material M,
    struct load L, struct cylinder C, float G[], float XW[],
    float *OBJ, FILE *out, int SSP, int mflag, char LEB);

/** corsstol controls the optimization loop of the sensitivity study
called from: main
calls      : CTPinput, EVALTOL, DOT, TOLOUT, and CTfinalout
returns    :
    INFO, IPRM, IWK, j, MINMAX, NRIWK,NRWK,RPRM,WK,OBJ - as in ctopt
    Gslope - d(constraint)/d(design variable)
    GW - array of worst condition constraints, used in DOT
    Wslope - d(weight)/d(design variable)
    Xnorm - array of design variable normalizing values
    AddTol - additonal tolerances gained by constraints not equal to 0
    delWt - delta weights from each design variables tolerance
    DVmin, DVmax - new design variable tolerance limits, based on AddTol
    OPTWT - the Minimum Material Condition weight
    NSTmin the minimum number of stringers within the tolerance;
****************************************************************************/
void corsstol(struct stringer S, struct ring R, struct material M,
        struct load L, struct cylinder C, int hap, long int IPRINT,
        int l2ap, long int METHOD, long int NCON, long int NDV, int Nrng,
        int Nstap, FILE *out, int SSP, int tap, float Tol[], int tstap,
        int Wap, float X[], float XL[], float XU[], int mflag, char LEB);

/** CTPinput prints the tolerances to the output file
called from: corsstol
calls      :
returns    :
*************************************************************************/
void CTPinput(char stype, float Tol[], FILE *out);

/** CTfinalout prints the tolerancing information
called from: corsstol
calls      :
returns    :
        i,j - indices
        ERR - used to mark driving constraints
        string1,string2 - used for outputing diffent strings
        NSTmax - maximum number of stringers within the tolerance
*************************************************************************/
void CTfinalout(float OPTWT, float MAXWT,
        float WSLOPE[], float DELWT[],     float Gslope[][6],
        float GW[],     float ADDTOL[][6], float DVMIN[],
        float DVMAX[],  char stype,        FILE *out,
        float Tol[],    float R,           float h,
        float l2,       float tst,         float b,
        float t,    float W, float Nst, float NSTmin,
        char LEB, long int NCON);

/** EVALTOL controls the sensitivity analysis
called from: corsstol
calls      : CALLeval, WEIGHT
returns    : weight, GSLOPE, and GW
        f - worst case control flag
        G - working constraints
        Gsign - array indicating positive or negative slope of GSLOPE
        GWdone - completed GW calculating flags
        INITG - reference set of constraint values
        INITX - reference set of design varialbes
        j,i,k - indices
        OBJ - dummy weight variable
        XW - worst case set of design variables for a given constraint
*************************************************************************/
float EVALTOL(struct stringer S, struct ring R, struct material M,
        struct load L, struct cylinder C, float GSLOPE[][6], float GW[],
        int hap, int l2ap, int Nstap, FILE *out, int tap, float Tol[],
        int tstap, int Wap, float X[], float XNORM[], int SSP, int mflag,
        char LEB, long int NCON);

/** TOLOUT performs the sensitivity calculations
called from: corsstol
calls      : WEIGHT
returns     : AddTol, delWt, additional max and min design variables,
                the minimum material condition weight, and WSLOPE
        i,j - indices
        MAXWT - maximum material condition weight
        NEWDVS, NEWDVL - minimum and maximum holding variables for additional
                design variable range
        Nmin - used to calculate sensitivities to stringer spacing
        OBJH - comparison weight for calculating weight slopes
        TW array of worst tolerances
        XW - working design variable array, Nst is replaced by b
*************************************************************************/
void TOLOUT(float ADDTOL[][6], float ALP, float AR, float DELWT[],
        float DVMAX[], float DVMIN[], float FWT, float L,
        float GSLOPE[][6], float GW[], float L1, float NRNG,
        float *OPTWT, float R, float RHO, char STYPE,
        float TOL[], float WSLOPE[], float X[], float ZR,
        int harpos, int l2arpos, int tstarpos, int Nstarpos,
        int tarpos, int Warpos, float NSTmin, long int NCON);

/** WEIGHT calculates the weight of the cylinder
called from: EVALTOL, TOLOUT,
calls      :
returns    : weight of the structure
    float ALPHA,HA,AS,RASL,RISL;
        ALPHA - web angle in radians
        HA - web slant length
        AS - stringer cross sectional area
        RASL - radius at the center of the skin
        RISL - radius at the inner skin line
*************************************************************************/
float WEIGHT (float A, float C, float D, float EE, float FF,
    float FWT, float L, float L1, float NRNG, float P,
    float R, float RHO, float stype, float ALP, float AR,
    float ZR);

/************************************************************************
************************* file colstress.c ******************************
************************************************************************/

/** COLSTRESS calculates the stress in the stringer and does the stress
analysis if the skin buckles and redistributes the load
called from: eval
calls : loopI
returns : column buckling constraint value, adjusted cylinder area and
    tension stress, stress in the column, and the column buckling stress
must be called after stringer, skinbuck, and getScrip
must be called befor stresscheck
    be - effective skin width
    Ise - skin + stringer moment of inertia
    radg - skin + stringer radius of gyration
    ScrstcolJE - critical Johnson-Euler column stress
    tmp- effective width temporary location
    We0 - minimum effective skin width
***************************************************************************/
float colstress(float *Anew, struct stringer S, float d, float E,
            struct load L, struct cylinder C, float Faxial, float Io, FILE *out,
            float *St, float *Scol, float Scrip, float Scrpl, float *Scrstcol,
            float Sskbd, int SSP, float taucr, float tauskbd, float Ysk[],
            float Yst[]);


/** LOOPI controls the iterations for calculating the cylinder properties
with the skin buckled
called from: colstress
calls : newI
returns : stringer column stress, new cross sectional area and tension
        stress, minimum effective skin width
        Acyl - cylinder cross sectional area
        count - iteration limiter
        I - cylinder moment of inertia
        Ioid - last iterations moment of inertia
        ybar - distance of cylinder neutral axis from the centerline
***********************************************************************/
float loopI(float *Anew, struct stringer S, float M, float sf, float E,
        struct cylinder C, float Faxial, float Io, FILE *out, float *St,
        float Scrpl, int SSP, float *We0, float Ysk[], float Yst[]);


/** NEWI calculates the new cylinder moment of inertia and neutral axis
called from: loopl
calls :
returns : adjusted area, moment of inertia and neutral axis location
        of the cylinder, and the minimum effective skin width
    A - ineffective skin area
    i - index
    N - 1/2 number of stringers (symmetric)
    Slb - stress carried in buckled skin
    Ssk, Sst - stress in individual skin segments and stringers
    sumA, sumAy, sumAyy - summations of area, area*distance from
    neutral axis, and area*distance**2
    tmp- temporary effective skin width location
    We - effective skin widths
**************************************************************************/
void newI(float Acyl, float *Anew, struct stringer S, float M, float sf,
        float t, float E, float Faxial, float *I, float Io, FILE *out,
        float Scrpl, int SSP, float *We0, float *ybar, float Ysk[],
        float Yst[]);

/************************************************************************
************************* file gencyl.c ******************************
************************************************************************/
/** gammaF and gammaM calculate the axial and bending knockdown factors
called from: GCBcalc
calls
returns : the knockdown factor
************************************************************************/
float gammaF(struct stiffness ES, float r);
float gammaM(struct stiffness ES, float r);



/** Pcrcalc calculates the critical pressure buckling load
called from: GCBcalc
calls : getA
returns : critical pressure and number of hoop waves
    n - working number of hoop waves
    P - working pressure
    Pcr - critical pressure
    Pold - last iterations critical pressure
**************************************************************************/
float Pcrcalc(struct stiffness egdck, float r, float l, int *ncr, FILE *out,
        int SSP);



/** GETA calculates det(A[3] [3])/det(A[2] [2])
called from: gencyl, Pcrcalc
calls
returns : result from division of the determinates
    AII..A33 - A array locations
    detA3x3 - determinate of the full three by three array
    detA2x2 - determinate of [All AI2 / A21 A22] only
************************************************************************/
float getA(struct stiffness ES, float r, float l, int m, int n, FILE *out,
        int SSP);



/** GETSTIFFNESSES calculates the smeared orthotropic shell properties
called from: GCBcalc
calls :
returns : the smeared orthotropic cylinder properties
    Er, Es - Young's Modulus of the rings and stringers
    Gr, Gs - Shear Modulus of the rings and stringers
************************************************************************/
void getstiffnesses(struct ring R, struct stringer S, struct material M,
        struct cylinder C, struct stiffness *egdck, FILE *out,
        int SSP);




/** GENCYL calculates the general cylinder buckling critical load
called from: GCBcalc
calls : getA
returns : critical line load and the hoop and axial waves
        m, n - working values of axial and hoop waves
        mm, nm - tracks minimum numbers of waves
        Nm - tracks minimum line load
        Nmt - temporary critical line load
        Nold - last iterations line load
**********************************************************************/
void gencyl(float l, float Nr, float r, struct stiffness egdck, int *mmin,
        int *nmin, float *Nx, FILE *out, int SSP);

/** GCBcalc controls the general cylinder buckling analysis
called from: eval
calls : getstiffnesses, gencyl, gammaM, gammaF, and Pcrcalc
returns : the shell buckling constraint, the number of axial and hoop
        waves in the buckled mode, the critical line load and
        pressure, the adjusted line load, the number of hoop
        waves from pressure buckling, the shell buckling flag,
        and the knockdown factors
    egdck - structure of smeared orthotropic shell properties
    G1 - temporary constraint value
***********************************************************************/
float GCBcalc(struct ring R, struct stringer S, struct material M,
        struct cylinder C, struct load L, float G[], FILE *out, int SSP,
        int *mgcb, int *ngcb, float *Nx, float *Pcr, float *N,
        float Io, float Faxial, int *ncr, char *GCB, float *gamF,
        float *gamM);




/************************************************************************
************************* file lebcrip.c ******************************
************************************************************************/

/** GETSCRIP calculates elastic buckling, if needed, and inelastic crippling
called from: eval
calls : findICBuck
returns : Scrip for colstress,
        and i or 2 critical constraint values for DOT
must be called before colstress
    alpha - web angle in radians
    m - I stringer coupled buckling wave number
    S1 - crippling stress, or I flange elastic buckling stress, or
        hat flange elastic buckling stress
    S2 - I coupled buckling stress, hat web local buckling (if needed)
    S3 - flange crippling stress
    S4 - web crippling stress
******************************************************************************/
float getScrip(struct stringer S, struct material M, FILE *out,
        float *Scrip1, float *Scrip2, float *ScripS, int SSP, int *mdrv,
        float l, int mflag, char LEB);



/** findICBuck controls the search for the lowest coupled buckling stress
called from: getScrip
calls : secant
returns : critical stress with the associated number of waves
        i - index
        m - number of waves
        S - critical stress variable
        Sdrv - lowest critical stress
        Slast - critical stress from the previous iteration
*************************************************************************/
float findICBuc(int mflag, int *mdrv, float l, float tst, float nu, float h,
        float l2, float W, float E, int SSP, FILE *out);



/** secant performs the secant method root solver
called from: findICBuck
calls : G6eqn
returns : critical stress from a given number of waves
        count - iteration limiter
        Fj, Fi - function values on different iterations
        hold - used to switch Si and Sj
        i - index
        Smin, Smax - bounds of critical stress range
        Si, Sj - stress values on different iterations
******************************************************************************/
float secant(int m, float l, float tst, float nu, float h, float l2, float W,
        float E);


/** G6eqn is the defining equation for I coupled buckling
called from: secant
calls
returns : value of the function
*******************************************************************************/
float G6eqn(float S, float m, float l, float tst, float nu, float h,
        float l2, float W, float E);


/** domainexit exits the program when to prevent a sqrt error which comes
from having a negative web length
called from: getScrip
calls :
returns : ends the program
******************************************************************************/
void domainexit(float l2, float l1, float h, FILE *out);

/************************************************************************
************************* file skinbuck.c ******************************
************************************************************************/
/** SKINBUCK calculates the skin buckling constraint
called from: eval
calls : Ks, Kc
returns : G[0] for DOT
        Scrpl, Sskbd, tauskbd, taucr for colstress
        Stsk, tau, tau0 for stresscheck
must be called before stresscheck, and colstress
        alpha, K, K1 - pressure stabilization factors
        bpl - distance between fixed points on the skin
        gO, g0max - design ratio, and maximum design ratio
        i - index
        Kcr, Ksh - axial and shear plate buckling constants
        q, qmax - shear flow between stringers and maximum shear flow
        Ssk - stress in the skin
        Z - non dimensional parameter for calculating Kcr and Ksh
**************************************************************/
float skinbuck(struct stringer S, float d, struct material M,
        struct load L, struct cylinder C, float Faxialp, float Io, FILE *out,
        float *Scrpl, float *Sskbd, int SSP, float *Stsk, float *tau,
        float *tau0, float *taucr, float *tauskbd, float Ysk[], float Yst[]);



/** Kc interpolates between curves for the skin buckling constants
called from: skinbuck
calls : Kc## functions
returns : skin buckling constant
        Kc## functions return the skin buckling ratio for the given r/t ratio
        Ks returns the shear skin buckling constant
********************************************************************/
float Kc(float Z, float r, float t);
float Kc100(float Z);
float Kc300(float Z);
float Kc500(float Z);
float Kc1000(float Z);
float Kc1500(float Z);
float Ks(float Z);

/************************************************************************
************************* file stress.c ******************************
************************************************************************/

/* VMSStress calculates Von Mises Stress given Sx, Sy, and Tauxy
called from: stresscheck
calls :
returns : Von Mises stress
    S1, S2 plane stress principle stresses
    VMS - Von Mises stress
*******************************************************************/
float VMStress(char loc[], FILE *out, int SSP, float Sx, float Sy, float tauxy);


/* STRESSCHECK checks the Von Mises Stress at three locations
called from: eval
calls : VMStress
returns : highest Von Mises stress
must be called after stringer, colstress, and skinbuck
    risl - radius at the inner skin line
    VMSA, VMSB, VMSC - Von Mises stress at the three locations
*******************************************************************/
float stresscheck(float Anew, float As, float Ns, float Ph, float r, float t,
        float Faxial, FILE *out, float St, float Scol, int SSP, float tau,
        float tau0, float sf);




/************************************************************************
************************* file stringer.c ******************************
************************************************************************/
/** STRINGER calculates the stringer cross sectional properties
called from: eval
calls : stringerP
returns : S.A for stresscheck,
    S.A, S.la, S.I, and S.Z for colstress,
    S.A, S.la for skinbuck
    S.A, S.I, S.Z, S.J for GCBcalc
must be called before stresscheck, colstress, GCBcalc, or skinbuck
    a - array of segment areas
    ad - (segment areas)*(segment distance from skin)
    add - (segment areas)*(segment distance from skin)**2
    alpha - web angle in radians
    d - array of segment distances from the skin
    ha - hat stringer web slant height
    i - index
    I - array of segment moments of inertia
    ix, iy - stringer moments of inertia parallel and perpendicular to
            the skin, respectively
    na - number of stringer segments
    sumad, sumadd, sumi - summations of the ad, add, and I arrays
    xbar,ybar - stringer neutral axis distances from the skin and the
                side of the stringer, respectively
*********************************************************************/
void stringer(struct stringer *S, struct cylinder C, FILE *out, int SSP);


/** STRINGERP prints section information to the output file
called from: stringer
calls :
returns :
**********************************************************************/
void stringerP(int na, float a[], float d[], float ad[], float add[],
        float I[], float A, float bar, float i, char line[],
        FILE *out);

