#include "corsstol.h"

long int initDVs(float XL[], float X[], float XU[], int *hap, int *l2ap,
        int *tstap, int *Nstap, int *tap, int *Wap, float Xinit[],
        long int ndv)
{
    int i;
    long int NDV = ndv;
    int pos = 0;
    if ( (XL[pos] == X[pos]) && (X[pos] == XU[pos]) )
        *hap = Xadjust(6,pos,&NDV, X,Xinit,XL,XU);
      else *hap = pos++;

    if ( (XL[pos] == X[pos]) && (X[pos] == XU[pos]) )
        *l2ap = Xadjust(7,pos,&NDV, X,Xinit,XL,XU);
      else *l2ap = pos++;

    if ( (XL[pos] == X[pos]) && (X[pos] == XU[pos]) )
        *tstap = Xadjust(8,pos,&NDV, X,Xinit,XL,XU);
      else *tstap = pos++;

    if ( (XL[pos]==X[pos]) && (X[pos]==XU[pos]))
        *Nstap = Xadjust(9,pos,&NDV, X,Xinit,XL,XU);
      else *Nstap = pos++;

    if ( (XL[pos]==X[pos]) && (X[pos]==XU[pos]))
        *tap = Xadjust(10,pos,&NDV, X, Xinit,XL,XU);
      else *tap = pos++;

    if ((0==XL[pos]) && (0==X[pos]) && (0==XU[pos]))
    { *Wap = *tstap; NDV--; }
    else
        if ( (XL[pos]==X[pos]) && (X[pos]==XU[pos]))
            *Wap = Xadjust(11,pos,&NDV, X,Xinit,XL,XU);
        else *Wap = pos;
    for (i=0; i<NDV; i++)
{
    Xinit[i] = X[i];
    XL[i] = XL[i]/Xinit[i];
    XU[i] = XU[i]/Xinit[i];
    X[i] = 1.0;
}
return (NDV);
} /************************************** end initDVs *******/


void comlineerr()
{
    printf("\nUsage: C:\>CORSS  file.in  file.out");
    fcloseall();
    exit(1);
}


int Xadjust(int newpos, int pos, long int *NDV, float X[], float Xinit[],
            float XL[], float XU[])
{
    int k;
    X[newpos] = X[pos];
    Xinit[newpos] = 1.;
    for (k=pos; k<(*NDV-1); k++) {
         XL[k] = XL[k+1]; X[k] = X[k+1]; XU[k]=XU[k+1];
         }
    *NDV -= 1;
    return(newpos);
}
