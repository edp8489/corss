#include "corsstol.h"

void main(int argc, char *argv[])
{
  FILE *in,*out;
  long int NDV, NCON, IPRINT, METHOD;
  float X[12],XL[6],XU[6],G[5],OBJ;
  float Xinit[12],Tol[6];
  char Title[80],LEB;
  int i,SSP,hap, l2ap, tstap,Nstap,tap,Wap,mflag;
  struct material M;
  struct load L;
  struct stringer S;
  struct ring R;
  struct cylinder C;
  printf("\nmain\n");
  NDV = 6; NCON = 5;
  if ( (in = fopen(argv[1],"rt")) == NULL)
  {
      printf("\ncan't open input file, First parameter must be input file");
      comlineerr();
  }
  if (strcmp(argv[1],argv[2]) == 0)
  {
      printf("\ninput filename is the same as output filename");
      comlineerr();
  }
  if ( (out = fopen(argv[2],"wt")) == NULL)
  {
      printf("\ncan't open output file, Second param. must be output file");
      comlineerr();
  }
  fprintf(out,"CORSS - Cylinder Optimization of Rings, Skin and Stringe");
  fprintf(out,"rs\n    NASA/MSFC/ED52 Structural Development Branch");
  fprintf(out,"\n    Jeff Finckenor, Sep. 1993, ver. 2.1");
  fprintf(out,"\n    Copyright (c) 1994 National Aeronautics and");
  fprintf(out,"\n    Space Administration. No copyright claimed in");
  fprintf(out,"\n    USA under Title 17, U.S. Code. All other rights");
  fprintf(out,"\n    reserved.\n");
  fprintf(out,"\n    Modified by Eric Peters, Sept 2017, to use NLOPT");
  fprintf(out,"\n    open-source optimization library in lieu of DOT\n");

  printf("CORSS - Cylinder Optimization of Rings, Skin and Stringe");
  printf("rs\n    NASA/MSFC/ED52 - Structural Development Branch");
  printf("\n    Jeff Finckenor, Sep. 1993, vet. 2.1");
  printf("\n    Copyright (c) 1994 National Aeronautics and");
  printf("\n    Space Administration. No copyright claimed in");
  printf("\n    USA under Title 17, U.S. Code. All other rights");
  printf("\n    reserved.\n");
  printf("\n    Modified by Eric Peters, Sept 2017, to use NLOPT");
  printf("\n    open-source optimization library in lieu of DOT\n\n");

  readinput(Title,in,&IPRINT,&METHOD,&SSP,&M,&C,&S,&R,&L,XL,X,XU,Tol,&mflag,&LEB);
  fclose(in);

  fprintf(out,"\n%s",Title);

  if (0 == R.N) { R.A = .001; R.I = .001; R.Z = .001; R.J = .001; }

  pinput(S,R,M,L,C,IPRINT,METHOD,out,SSP,X,XL,XU,mflag,LEB);

  if ( (S.stype != "H") && (S.stype != "I") )
  {
      printf("\n\n%c is not a valid stringer type\a\a\a",S.stype);
      exit(4);
  }

  if (('H'==S.stype) && ('N'!=LEB)) NCON--;

  NDV = initDVs(XL,X,XU,&hap,&l2ap,&tstap,&Nstap,&tap,&Wap,Xinit,NDV);

  if (NDV != 0)
      ctopt(SSP,out,X,Xinit,hap, l2ap, tstap, Nstap,tap,Wap,S,R,M,L,C,G,
            &OBJ,&METHOD,&IPRINT,&NDV,&NCON, XL,XU,mflag,LEB);
  fprintf(out,"\n");
  fputs(Title,out);

  if ( (SSP == 1) || (SSP == 3) ) SSP = 4; else SSP = 5;
  CALLeval(S,R,M,L,C,G,X,Xinit,&OBJ,out,SSP,hap, l2ap,Nstap, tap,
              tstap,Wap,mflag,LEB);

  for (i=0; i<NCON; i++)
      if (G[i]>0)
      {
      fprintf(out,"\n\n** WARNING G[%d] (=%7.5f>0) IS NOT ",i,G[i]);
      fprintf(out,"SATISFIED, THIS IS NOT A VALID SOLUTION!");
      }
  if ( (Tol[0] !=0) || (Tol[1]!=0) || (Tol[2]!=0) || (Tol[3]!=0) ||
      (Tol[4]!=0) || (Tol[5]!=0))
  {
//      fprintf(out,"\n\n** WARNING: TOLERANCE SENSITIVITY NOT SUPPORTED AT THIS TIME");
      for (i=0; i<NDV; i++)
      { XL[i] *= Xinit[i]; XU[i] *= Xinit[i]; X[i] *= Xinit[i]; }
      corsstol(S,R,M,L,C,hap, IPRINT, l2ap,METHOD,NCON, NDV, R.N, Nstap,out,
      SSP, tap,Tol,tstap,Wap,X,XL, XU,mflag,LEB);
      // corsstol must define IPRM, IWK, MINMAX, NRIWK, NRWK, RPRM, WK

  }
  fclose(out);
  printf("\a\a\a");
//  return 0;
} /*************************************************************** end main */
