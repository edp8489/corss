#include "corsstol.h"
#include "nlopt_interface.h"

void CALLeval(struct stringer S, struct ring R, struct material M,
        struct load L, struct cylinder C, float G[], float X[],
        float Xinit[], float *OBJ, FILE *out, int SSP, int hap, int l2ap,
        int Nstap, int tap, int tstap, int Wap, int mflag, char LEB)
{
    S.h = X[hap]*Xinit[hap];
    S.l2 = X[l2ap]*Xinit[l2ap];
    S.N = X[Nstap]*Xinit[Nstap];
    S.t = X[tstap]*Xinit[tstap];
    C.t = X[tap]*Xinit[tap];
    S.W = X[Wap]*Xinit[Wap];
    eval(S,R,M,L,C,G, OBJ,out,SSP,mflag,LEB);
} /******************************************* end CALLeval ****/



void ctopt(int SSP, FILE *out, float X[], float Xinit[], int hap,
            int l2ap, int tstap, int Nstap, int tap, int Wap, struct stringer S,
            struct ring R, struct material M, struct load L, struct cylinder C,
            float G[], float *OBJ, long int *METHOD, long int *IPRINT,
            long int *NDV, long int *NCON, float XL[], float XU[], int mflag,
            char LEB)
{
    long int INFO, IPRM[20],IWK[110],j,MINMAX,NRIWK,NRWK;
    float RPRM[20],WK[450];
    int iter, i;
    for (j=0; j<20; j++)
    {
        RPRM[j] = 0.0;
        IPRM[j] = 0;
    }
    NRWK = 450; /* make the dimension of IWK and WK the same */
    NRIWK = 110;
    MINMAX = -1;
    INFO = 0;
    if ( (SSP == 2) || (SSP == 3) )
    {
        fprintf(out,"\n\n OPTIMIZATION HISTORY\n\n");
        fprintf(out,"h    l2    tst    Nst    t    W    skin    shell ");
        fprintf(out,"stres Crip1 Crip2 Wt   It");
    }
    iter = 0;
    do
    {
        if (IPRM[18]!= -1)
        {
            printf("\nIteration #%i",IPRM[18]);
            iter= (int) IPRM[18];
        }
         if ( (SSP ==2) || (SSP ==3) )
         {
             fprintf(out,"\n%5.4f %5.4f %5.4f %5.1f %5.4f %5.4f ",
                    X[hap]*Xinit[hap], X[l2ap]*Xinit[l2ap],
                    X[tstap]*Xinit[tstap], X[Nstap]*Xinit[Nstap],
                    X[tap]*Xinit[tap], X[Wap]*Xinit[Wap]);
            fflush(out);
         }
         CALLeval(S,R,M,L,C,G,X, Xinit,OBJ,out,SSP,hap, l2ap,Nstap,tap,tstap,Wap,
                mflag,LEB);
        for (i=0; i<(*NCON); i++) G[i]+=.005;
        // TODO: replace DOT call with NLOpt call
        DOT(&INFO,METHOD, IPRINT,NDV, NCON, X, XL,XU,OBJ,&MINMAX,G, RPRM, IPRM,
            WK,&NRWK, IWK,&NRIWK);
        printf(" %3.1f %7.4f %7.4f %7.4f %7.4f %7.4f",
                *OBJ,G[0],G[1],G[2],G[3],G[4]);
        if ( (SSP == 2) || (SSP == 3) )
        {
            fprintf(out,"%5.2f %5.2f %5.2f %5.2f %5.2f",G[0],G[1],G[2],G[3],G[4]);
            fprintf(out," %4.0f %d",*OBJ, iter);
        }
    } while (INFO!=0); /*** end optimization (INFO=0) loop ***/
    fprintf(out,"\n\nNumber of Iterations to Optimize = %d",iter);
    if (iter > 20) fprintf(out," -- WARNING -- Solution may not have converged.");
} /******************************************************** end ctopt ****/

/** stand-in DOT function to verify the original code compiles and runs **/
void DOT(long int *INFO, long int *METHOD, long int *IPRINT,
                long int *NDV, long int *NCON, float X[],
                float XL[], float XU[], float *OBJ,
                long int *MINMAX, float G[], float RPRM[],
                long int IPRM[], float WK[], long int *NRWK,
                long int IWK[], long int *NRIWK)
{
    // set value of INFO to 0 to spoof optimization loop into thinking it's complete
    *INFO = 0;
}

/**
* this is a modified version of the ctopt method to account for the syntax differences of NLopt
**/
void ctoptNL(int SSP, FILE *out, float X[], float Xinit[], int hap,
            int l2ap, int tstap, int Nstap, int tap, int Wap, struct stringer S,
            struct ring R, struct material M, struct load L, struct cylinder C,
            float G[], float *OBJ, long int *METHOD, long int *IPRINT,
            long int *NDV, long int *NCON, float XL[], float XU[], int mflag,
            char LEB)
{
    long int INFO, IPRM[20],IWK[110],j,MINMAX,NRIWK,NRWK;
    //float RPRM[20],WK[450];
    int iter, i;

    /* DOT variables not needed by NLopt
    for (j=0; j<20; j++)
    {
        RPRM[j] = 0.0;
        IPRM[j] = 0;
    }
    NRWK = 450; // make the dimension of IWK and WK the same
    NRIWK = 110;
    MINMAX = -1;
    */
    INFO = 0;
    if ( (SSP == 2) || (SSP == 3) )
    {
        fprintf(out,"\n\n OPTIMIZATION HISTORY\n\n");
        fprintf(out,"h    l2    tst    Nst    t    W    skin    shell ");
        fprintf(out,"stres Crip1 Crip2 Wt   It");
    }
    iter = 0;

    // create NLopt object
    nlopt_opt opt;

    // set nlopt algorithm
    if (METHOD == 0 || METHOD == 1){
    opt = nlopt_create(NLOPT_LN_COBYLA, NDV);
      }
      else if (METHOD == 2){
        // choose another method to use
        *INFO = -1;
        break;
      }
      else {
        // do not integrate
        *INFO = -1;
        break;
      }

    // create struct of additional args for objective function
    func_data fd = {};

    // create struct of additional args for constraint function
    con_data cd = {};

    // set NLopt objective function
    nlopt_result nlopt_set_min_objective(opt, ct_func, fd);

    // assign vector constraint function to object
    nlopt_add_inequality_mconstraint(opt, (unsigned) m, ct_mcons, cd, 1e-8);

    //set lower bound
    nlopt_set_lower_bounds(opt, XL);

    // set upper bound
    nlopt_set_upper_bounds(opt, XU);

    // begin the optimization
    double minf;
    if (nlopt_optimize(opt, &X, &minf) < 0) {
    printf("nlopt failed!\n");
    }
    else {
        printf("found minimum at f(%g,%g) = %0.10g\n", x[0], x[1], minf);
    }


    // destroy nlopt object when optimization is complete
    void nlopt_destroy(opt);

    fprintf(out,"\n\nNumber of Iterations to Optimize = %d",iter);
    if (iter > 20) fprintf(out," -- WARNING -- Solution may not have converged.");
} /******************************************************** end ctoptNL ****/
