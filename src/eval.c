#include "corsstol.h"

void eval(struct stringer S, struct ring R, struct material M, struct load L,
        struct cylinder C, float G[], float *OBJ, FILE *out, int SSP,
        int mflag, char LEB)
{
    float Anew, dtheta, Faxial, Faxialp, Io, N, Nx, Pcr, St,
        Scol, Scrip1, Scrip2, ScripS, Scrpl, Scrstcol,
        Ssk, Sskbd, tau, tau0, taucr, tauskbd, theta, Ysk[541], Yst[1081],
        gamF, gamM;
int i, mgcb, ncr, ngcb, mdrv;
char GCB;
putchar('\n');
putchar('E');
if (S.N>1080)
{
    printf("\n\nNumber of stringers exceeds 1080, ");
    printf("Number of stringers set to 1080");
    S.N = 1080;
    if (4 == SSP)
    {
        fprintf(out,"\n\nNumber of stringers exceeds 1080, ");
        fprintf(out,"Number of stringers set to 1080");
    }
}
if (SSP == 4) fprintf(out,"\n\nSTRINGER PROPERTY CALCULATIONS");
stringer(&S,C,out,SSP);
S.Z *= S.MZs;
R.d = C.l/(R.N+1);
S.b = 2*pi*C.r/S.N;
Anew = 2*pi*C.r*C.t + S.A*S.N;
if (L.Pa<0){
    Faxial = (L.F-L.Pa*pi*C.r*C.r)*L.sf/Anew;
    Faxialp = (L.F-L.Pa*pi*C.r*C.r)*L.sfp/Anew;
}
else {
    Faxial = (L.F*L.sf-L.Pa*pi*C.r*C.r)/Anew;
    Faxialp = (L.F*L.sfp-L.Pa*pi*C.r*C.r)/Anew;
}
Io = pi*C.t*C.r*C.r*C.r + S.N*S.I;
theta = 0; dtheta = 2*pi/S.N;
for (i=0; i<(S.N/2+1); i++){
    Yst[i] = (C.r+S.Z)*cos(theta);
    Ysk[i] = C.r*cos(theta);
    Io += 2*S.A*Yst[i]*Yst[i];
    theta+=dtheta;
}
St = -L.M*L.sf*C.r/Io + Faxial; /* tension stress */
if (SSP == 4){
    fprintf(out,"\n\nOVERALL CYLINDER CALCULATIONS");
    fprintf(out,"\n\nIo = %-22.0f   Tension Stress = f(Io) = %2.0f",Io,St);
    fprintf(out,"\nCross sectional area = %f",Anew);
    fprintf(out,"\nFaxial = f(F, Pa) = %8.1f   Faxialp = %8.1f",Faxial,Faxialp);
    fprintf(out,"\nRing Spacing = %-12.4f   Stringer Spacing = %7.4f",R.d,S.b);
    fprintf(out,"\n\nSKIN BUCKLING STRESS AND MAX SHEAR CALCULATIONS");
}
G[0] = skinbuck(S,R.d,M,L,C,Faxialp, Io, out,&Scrpl,&Sskbd, SSP,&Ssk,&tau,&tau0,
                &taucr,&tauskbd,Ysk,Yst);
if (SSP == 4) fprintf(out,"\n\nSTRINGER CRIPPLING CALCULATIONS\n");
getScrip(S,M, out,&Scrip1,&Scrip2,&ScripS,SSP,&mdrv, C.l,mflag,LEB);
if (SSP == 4) fprintf(out,"\n\nSTRINGER COLUMN BUCKLING CALCULATIONS");
G[1] = colstress(&Anew, S,R.d,M.E,L,C,Faxial,Io,out,&St,&Scol,ScripS,Scrpl,
                &Scrstcol,Sskbd,SSP, taucr, tauskbd,Ysk,Yst);
GCB = 'N';
G[3] = Scol/Scrip1 -1.;
G[4] = Scol/Scrip2 -1.;
if (Scol<0){
    fprintf(out,"\n\n\nWhole Cylinder is in Tension due to pressure, this");
    fprintf(out," is not a buckling problem.\n\n");
    exit(1);
}
if (SSP == 4) fprintf(out,"\n\nGENERAL CYLINDER BUCKLING CALCULATIONS");
G[1] = GCBcalc(R,S,M,C,L,G,out,SSP,&mgcb,&ngcb,&Nx,&Pcr,&N, Io,Faxial,&ncr,
            &GCB,&gamF,&gamM);
if (4 == SSP) fprintf(out,"\n\nVON MISES STRESS CHECK\n");
G[2] = stresscheck(Anew,S.A,S.N,L.Ph,C.r,C.t,Faxial,out,St,Scol,SSP,tau,tau0,
                    L.sf)/M.Scy - 1.;
*OBJ = (pi*2*C.r*C.t*C.l + S.A*C.l*S.N + R.A*R.N*2*pi*(C.r + R.Z))*M.rho + C.fwt;
if ( (SSP == 4) || (SSP == 5)){
    finalout(R,S,M,L,C,G,gamF,gamM,GCB,mgcb,N,ncr,ngcb,Nx,*OBJ,out,Pcr,Scol,
            Scrip1,Scrip2,Scrstcol,LEB,mdrv,Io,Faxial);
}
} /*********************************** end eval ***/
