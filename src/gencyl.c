#include "corsstol.h"

float gammaF(struct stiffness ES, float r)
{
return (1-.901*(1-exp(-sqrt(r/pow(ES.Dx*ES.Dy/ES.Ex/ES.Ey,.25))/29.8)));
}

float gammaM(struct stiffness ES, float r)
{
return (1-.731*(1-exp(-sqrt(r/pow(ES.Dx*ES.Dy/ES.Ex/ES.Ey,.25))/29.8)));
}

void getstiffnesses(struct ring R, struct stringer S, struct material M,
                    struct cylinder C, struct stiffness *egdck, FILE *out,
                    int SSP)
{
float Er, Es, Gr, Gs;
Es = M.E;
Er = M.E;
Gs = M.G;
Gr = M.G;
if (0 == R.N){
    R.d = 1.0E35;
}
egdck->Ex = M.E*C.t/(1-M.nu*M.nu) + Es*S.A/S.b;
egdck->Ey = M.E*C.t/(1-M.nu*M.nu) + Er*R.A/R.d;
egdck->Exy = M.nu*M.E*C.t/(1-M.nu*M.nu) ;
egdck->Gxy = M.E*C.t/2/(1+M.nu) ;
egdck->Dx = M.E*C.t*C.t*C.t/12/(1-M.nu*M.nu) + Es*S.I/S.b + S.Z*S.Z*Es*S.A/S.b;
egdck->Dy = M.E*C.t*C.t*C.t/12/(1-M.nu*M.nu) + Er*R.I/R.d + R.Z*R.Z*Er*R.A/R.d;
egdck->Dxy = M.E*C.t*C.t*C.t/6/(1+M.nu) + Gs*S.J/S.b + Gr*R.J/R.d;
egdck->Cx = S.Z*Es*S.A/S.b;
egdck->Cy = R.Z*Er*R.A/R.d;
egdck->Cxy = C.t*C.t*C.t/12/(1-M.nu*M.nu);
egdck->Kxy = egdck->Cxy;
if (SSP == 4)
{
    fprintf(out,"\n\nEx  = %14.4f  Ey  = %14.4f  Exy = %14.4f",egdck->Ex, egdck->Ey,egdck->Exy);
    fprintf(out,"\nDx  = %14.4f  Dy  = %14.4f  Dxy = %14.4f",egdck->Dx, egdck->Dy,egdck->Dxy);
    fprintf(out,"\nCx  = %14.4f  Cy  = %14.4G  Cxy = %14.4G",egdck->Cx, egdck->Cy,egdck->Cxy);
    fprintf(out,"\nGxy = %14.4f  Kxy = %14.4G",egdck->Gxy,egdck->Kxy);
}
} /************************** end getstiffnesses ****/

float getA(struct stiffness ES, float r, float l, int m, int n, FILE *out,
            int SSP)
{
float A11,A22,A33,A12,A21,A23,A32,A31,A13,detA3x3,detA2x2;
A11 = ES.Ex*m*m*pi*pi/l/l + ES.Gxy*n*n/r/r;
A22 = ES.Ey*n*n/r/r + ES.Gxy*m*m*pi*pi/l/l;
A33 = ES.Dx*pow( m*pi/l , 4 ) + ES.Dxy*pow( m*pi*n/l/r , 2 ) +
        ES.Dy*pow( n/r , 4 ) + ES.Ey/r/r + 2*ES.Cy/r*n*n/r/r +
        2*ES.Cxy/r*m*m*pi*pi/l/l;
A12= (ES.Exy+ES.Gxy)*m*pi/l*n/r;
A21 = A12;
A23 = (ES.Cxy + 2*ES.Kxy)*m*m*pi*pi/l/l*n/r + ES.Ey*n/r/r +
        ES.Cy*pow( n/r , 3 );
A32 = A23;
A31 = ES.Exy/r*m*pi/l + ES.Cx*pow(m*pi/l,3) +
        (ES.Cxy+2*ES.Kxy)*m*pi/l*n*n/r/r;
A13 = A31;
detA3x3 = A11*A22*A33 + A12*A23*A31 + A13*A21*A32
        - A13*A22*A31 - A11*A23*A32 - A12*A21*A33;
detA2x2 = A11*A22 - A12*A21;
if (SSP == 6)
{
    fprintf(out,"\n\n A = | %12.4f    %12.4f    %12.4f |",A11,A12,A13);
    fprintf(out,"\n     | %12.4f    %12.4f    %12.4f |",A21,A22,A23);
    fprintf(out,"\n     | %12.4f    %12.4f    %12.4f |",A31,A32,A33);
    fprintf(out,"\n\ndeterminant of A(3x3) = %G",detA3x3);
    fprintf(out,"\ndeterminant of A(2x2) = %G",detA2x2);
}
return( detA3x3/detA2x2 );
} /******* end getA ****/


float Pcrcalc(struct stiffness egdck, float r, float l, int *ncr, FILE *out,
        int SSP)
{
float P, Pcr, Pold;
int n;
Pcr = .75*r/l/l*getA(egdck,r,l,1,1,out,SSP);
for (n=2; n<150; n++)
{
    if ( (P = .75*r/n/n*getA(egdck,r,l,1,n,out,SSP)) < Pcr )
    {
        Pcr = P; *ncr = n;
    }
}
if (SSP == 4)
{
    fprintf(out,"\n\nCritical Pressure");
    P = getA(egdck, r,l,1,*ncr,out,6);
    fprintf(out,"\nCritical Pressure = %6.2f with %i circumferential waves",Pcr,*ncr);
    if ( *ncr>=149 )
    {
    fprintf(out,"\nWARNING: Program may not have calculated lowest ");
    fprintf(out,"critical pressure.  ncr may be greater than %d",*ncr);
}
}
return(Pcr);
} /************************************************* end Pcrcalc */




void gencyl(float l, float Nr, float r, struct stiffness egdck, int *mmin,
    int *nmin, float *Nx, FILE *out, int SSP)
{
float Nm, Nold,Nmt;
int m, n,mm, nm;
m = 1; n = 1;
mm = 1; nm = 1;
*mmin = 1; *nmin = 1;
Nm = l*l/m/m/pi/pi*getA(egdck, r,l,m,n,out,SSP);
Nold = Nm*2;
*Nx = Nm;
while ((*Nx)<Nold)
{
    Nold = (*Nx);
    n=4;
    Nm = l*l/m/m/pi/pi*getA(egdck, r,l,m,n,out,SSP);
    do
    {
        if ( (Nmt = l*l/m/m/pi/pi*getA(egdck, r,l,m,n,out,SSP)) < Nm )
        {
            Nm = Nmt; mm = m; nm = n;
        }
        n++;
    } while (Nm == Nmt);
    if (Nm < (*Nx) ) {
        *Nx = Nm; *mmin = mm; *nmin = nm;
    }
    m++;
}
if (SSP == 4)
{
fprintf(out,"\n\nCritical Line Load, m=%d, n=%d, Ncr=%f",*mmin,*nmin,*Nx);
Nmt = getA(egdck, r,l,*mmin,*nmin,out,6);
}
if ( ((SSP==4) || (SSP==5)) && ((*mmin != 1) && (m == Nr+1)) )
{
    fprintf(out,"\n\nWARNING! for the critical load case, there is one");
    fprintf(out,"axial half wave\nper section between rings. The rings");
    fprintf(out,"may not help\nsupport general cylinder buckling.");
}
} /************************************************* end gencyl */

float GCBcalc(struct ring R, struct stringer S, struct material M,
                struct cylinder C, struct load L, float G[], FILE *out, int SSP,
                int *mgcb, int *ngcb, float *Nx, float *Pcr, float *N,
                float Io, float Faxial, int *ncr, char *GCB, float *gamF,
                float *gamM)
{
struct stiffness egdck;
float G1 = 1000;
putchar('G');
getstiffnesses(R,S,M,C,&egdck, out,SSP);
if ((G[0]<0) || (L.sf<=L.sfp)) /* if skin doesn't buckle */
{
    gencyl(C.l,R.N,C.r,egdck,mgcb, ngcb, Nx,out,SSP);
    *gamM = gammaM(egdck,C.r);
    *gamF = gammaF(egdck, C.r);
    *N = (L.M*L.sf*C.r/Io*(S.A/S.b+C.t))/(*gamM)+ Faxial*(S.A/S.b+C.t)/(*gamF);
    if (4==SSP)
    {
    fprintf(out,"\n\nCritical line load is Ncr = %f",*Nx);
    fprintf(out,"\nKnockdown Adjusted Line Load is %f",*N);
    fprintf(out,"\nat axial waves m = %d, and hoop waves n = %d",*mgcb,*ngcb);
    fprintf(out,"\ngammaF = %f,  gammaM = %f",*gamF,*gamM);
    }
    G1 = (*N)/(*Nx) - 1;
    if (L.Ph<0.0)
    {
        *Pcr = Pcrcalc(egdck,C.r,C.l,ncr,out,SSP);
        G1 += -L.Ph*L.sf/(*Pcr);
        if (4==SSP){
            fprintf(out,"\n\nCritical crushing pressure Pcr = %f",*Pcr)
        } ;
    }
    if (G1<G[1])
    {
        G[1] = G1;
        *GCB = 'Y';
        if (4 == SSP){
            fprintf(out,"\nGeneral Cylinder Buckling provides support above Column Buckling");
        }
    }
    else{
        if (4==SSP) {
            fprintf(out,"\nColumn Buckling provides support above General Cylinder Buckling");
        }
    return ( G[1]);
    }
}
 if (4==SSP)
{
fprintf(out,"\nGeneral Cylinder Buckling not checked because of");
fprintf(out," buckled skin");
}
return (G[1]);
} /*********************** end GCBcalc ****/








