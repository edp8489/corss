#include "corsstol.h"

void domainexit(float l2, float l1, float h, FILE *out)
{
printf("\a\a\n\nTop flange thickness + bottom flange thickness");
printf(" > stringer height");
printf("\n%20f + %23f > %15f",l2,l1,h);
printf("\nThis will cause a SQRT domain error, please make sure");
printf("\nthe smallest Stringer Height is greater then the");
printf("\nBottom Flange Thickness plus the largest Top Flange");
printf(" Thickness");
fprintf(out,"\n\nTop flange thickness + bottom flange thickness");
fprintf(out," > stringer height");
fprintf(out,"\n%20f + %23f > %15f",l2,l1,h);
fprintf(out,"\nThis will cause a SQRT domain error, please make sure");
fprintf(out,"\nthe smallest Stringer Height is greater then the");
fprintf(out,"\nBottom Flange Thickness plus the largest Top Flange");
fprintf(out," Thickness");
fcloseall();
exit(2);
} /*************************** end domainexit ****/



float G6eqn(float S, float m, float l, float tst, float nu, float h,
            float l2, float W, float E)
{
float M, Nx, D, k1,alpha,beta,b,I,Ab, k2,k3;
M = m*m*pi*pi/l/l;
Nx = S*tst;
D = E*tst*tst*tst/12/(1-nu*nu);
k1 = sqrt(Nx*M/D);
alpha = sqrt(M+k1);
beta = sqrt(-M+k1);
b = h-l2;
I = l2*W*W*W/12;
Ab = W*l2;
k2 = E*I*sin(beta*b)*M*M;
k2 += D*(( beta*beta*beta+beta*(2-nu)*M)*cos(beta*b));
k2 -= Ab*S*sin(beta*b)*M;
k2 *= (alpha*alpha-nu*M)*sinh(alpha*b);
k3 = E*I*sinh(alpha*b)*M*M;
k3 -= D*((alpha*alpha*alpha-alpha*(2-nu)*M)*cosh(alpha*b));
k3 -= Ab*S*sinh(alpha*b)*M;
k3 *= (beta*beta+nu*M)*sin(beta*b);
return(k2+k3);
} /***************************** end G6eqn ***/


float secant(int m, float l, float tst, float nu, float h, float l2, float W,
            float E)
{
float Fj,hold,Fi,Smin,Smax, Si,Sj;
int i,count;
count=0;
Smin = Sj = m*m*pi*pi*E*tst*tst/l/l/12/(1-nu*nu)+1.;
Fj = G6eqn(Sj, (float)m,l,tst,nu,h,l2,W,E);
Si = 2*Sj;
Fi = G6eqn(Si, (float)m,l,tst,nu,h,l2,W,E);
while (((Fi<0)&&(Fj<0)) || ((Fi>0)&&(Fj>0)))
{
    Si *= 2;
    Fi = G6eqn(Si, (float)m,l,tst,nu,h,l2,W,E);
}
Sj = Si/2;
Smax = Si;
Fj = G6eqn(Sj, (float)m,l,tst,nu,h,l2,W,E);
do{
    if ( Si<Smin ){
        Si=Smax;
    }
    hold = Si;
    Fi = G6eqn(Si, (float)m,l,tst,nu,h,l2,W,E);
    if ((Si==Sj) || (Fi==Fj)) {
        break;
    }
    else{
        Si = Sj - Fj/( (Fi-Fj)/(Si-Sj));
    }
    Sj = hold;
    Fj = Fi;
    if (count++>20) {
        break;
    }
} while (fabs(Fi) > .001 );
return (Si);
} /****************************** end secant ****/



float findICBuc(int mflag, int *mdrv, float l, float tst, float nu, float h,
                float l2, float W, float E, int SSP, FILE *out)
{
float Sdrv, S,Slast;
int i,m;
m=0;
S = 1E30;
if (mflag==0)
{
    do
    {
        m++;
        Slast = S;
        S = secant(m,l,tst,nu,h,l2,W,E);
        if (4==SSP){
            fprintf(out,"\nm = %d, Critical Stress = %f",m,S);
        }
    } while ((S-Slast)<0);
    *mdrv = m-1;
    Sdrv = Slast;
}
else
{
    Sdrv = S;
    for (i=1; i<=mflag; i++)
    {
        S = secant(i,l,tst,nu,h,l2,W,E);
        if (S<Sdrv) {
            Sdrv=S;
            *mdrv=i;
        }
        if (4==SSP){
            fprintf(out,"\nm = %d, Critical Stress = %f",i,S);
        }
    }
}
return (Sdrv);
} /*************** end findICBuc *****/


float getScrip(struct stringer S, struct material M, FILE *out,
                float *Scrip1, float *Scrip2, float *ScripS, int SSP, int *mdrv,
                float l, int mflag, char LEB)
{
float S1,S2,S3,S4,alpha;
int m;
putchar('C');
alpha = S.alp*pi/180;
if (S.stype == 'I'){
    S2 = findICBuc(mflag,mdrv, l,S.t,M.nu,S.h,S.l2,S.W,M.E,SSP,out);
    if (4==SSP){
        fprintf(out,"\nCoupled Buckling Stress, %9.2f",S2);
    }
    S3=.569311 / pow(sqrt(M.Scy/M.E*(S.W/2)/S.l2), .812712 ) * M.Scy;
    if ((S.h-S.l2-S.l1)<=0){
        domainexit(S.l2,S.l1,S.h, out);
    }
    S4=1.387194/pow(sqrt(M.Scy/M.E*((S.h-S.l2-S.l1)/cos(alpha))/S.t),.807179)* M.Scy;
    if (S3 > M.Stu){
        S3 = M.Stu;
    }
    if (S4 > M.Stu){
        S4 = M.Stu;
    }
    *ScripS = S1 = ( S3*S.l2*S.W + S4*S.t*((S.h-S.l2-S.l1)/cos(alpha)) )/ ( S.A - S.l1*S.W );
    if (4==SSP){
        fprintf(out,"\nCrippling: Top Flange = %8.1f, Web = %8.1f",S3,S4);
    }
    if ('N' == LEB )
    {
        S1=.456*pi*pi/12*M.E/(1-M.nu*M.nu)*pow(S.l2/(S.W/2),2);
        if (4==SSP){
            fprintf(out,"\nFlange Elastic Buckling Stress, %9.2f",S1);
        }
    }
}
else
{
    if (S.l2>S.t){
        S3 = 1.387194 / pow(sqrt(M.Scy/M.E*(S.l2-S.t)/S.t), .807179 ) * M.Scy;
    }
    else{
        S3 = 0;
    }
    S4 = 1.387194/ pow(sqrt(M.Scy/M.E*((S.h-S.W-S.t)/cos(alpha))/S.t),.807179)*M.Scy;
    if (S3 > M.Stu){
        S3 = M.Stu;
    }
    if (S4 > M.Stu){
        S4 = M.Stu;
    }
    *ScripS = S1 = ( S3*S.W*S.l2 + 2*S4*S.t*((S.h-S.W-S.t)/cos(alpha)) ) /( S.A - S.l1*S.t*2 );
    if (4==SSP){
        fprintf(out,"\n\nCrippling: Top Flange = %8.1f, Web = %8.1f",S3,S4);
    }
    if ('N'==LEB)
    {
        if (S.l2 == 0){
            S.l2 = 1.E-6;
        }
        S1 = 3.29*M.E/(1-M.nu*M.nu)*pow(S.W/(S.l2-S.t),2);
        S2 = 3.29*M.E/(1-M.nu*M.nu)*pow(S.t/((S.h-S.W-S.t)/cos(alpha)),2);
        if (4==SSP)
        {
        fprintf(out,"\nCritical Local Elastic Buckling Stress:");
        fprintf(out,"\n     Top Flange = %9.2f, Web = %9.2f",S1,S2);
        }
    }
    else S2 = M.Stu;
}
if (4==SSP){
    fprintf(out,"\nWeighted Average Crippling Stress, %9.2f",*ScripS);
}
*Scrip1 = S1;
*Scrip2 = S2;
} /*************************** end getScrip ****/

