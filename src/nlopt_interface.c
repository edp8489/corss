#include <nlopt.h>

/**
 * Call function for NLopt optimization program
 *
 *
 * METHOD = 0 or 1 uses COBYLA
 * METHOD = 2 uses SLSQP
 * IPRINT - DOT screen output. 0=none, 7=most
 * NDV - number of design variables
 * NCON - number of constraints
 *
 * XL[], XU[] - lower and upper limits of design variables
 * X[] - array of design variables
 *
 * *OBJ - value of objective function
 *
 * G - array of optimization constraints
 *
 * INFO - DOT completion flag
 * IPRM, IWK, RPRM, WK - DOT working arrays
 * MINMAX - DOT minimization/maximization flag
 * NRIWK, NRWK - IWK and WK sizes
 *
 **/

/**
* NLopt reference manual: https://nlopt.readthedocs.io/en/latest/NLopt_Reference/
*
**/
// objective function
double ct_func(unsigned n, const double* x, double* grad, void* func_data)
{
    // TODO
}

// multi-constraint function
void ct_mcons(unsigned m, double *result, unsigned n, const double* x, double* grad, void* f_data)
{
    // TODO
}

