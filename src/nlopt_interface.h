/** structure containing additional data to pass to ct_func which then gets
*   passed into CALLeval
**/
typedef struct {
// TODO
} func_data;

/** structure containing additional data to pass to ct_mcons to return
*   multi constraints
**/
typedef struct {
// TODO
} con_data;

/** Interface function to ctopt CALLeval, formatted to be compatible with
 * NLopt input
 * The return value should be the value of the function at the point x,
 * where x points to an array of length n of the optimization parameters.
 */
double ct_func(unsigned n, const double* x, double* grad, void* func_data);

/** function to collect all of the NCON constraint values into a single array
 * and pass them to the NLopt optimization routine
 *
 * m: NCON
 * n: NDV
 * x: vector of design points
 * grad: gradient of main function (if available - in this case is NULL)
 * func_data: used to pass additional info to function for evaluation
 * 		array of arbitrary data type which must be typecast inside the function
 * 		pointers to all constraint variables operated on by CALLeval
 *
 * An inequality constraint corresponds to ci <= 0 for 0 <= i < NCON
 **/
void ct_mcons(unsigned m, double *result, unsigned n, const double* x, double* grad, void* c_data);


/** copy of ctopt method, reformatted to use
 * NLopt optimization library instead of DOT
 * Method = 0 or 1 uses COBYLA
 * Method = 2 uses SLSQP
 *
 * Designed to have same inputs as original ctopt method
 *
 * */
void ctoptNL(int SSP, FILE *out, float X[], float Xinit[], int hap,
            int l2ap, int tstap, int Nstap, int tap, int Wap, struct stringer S,
            struct ring R, struct material M, struct load L, struct cylinder C,
            float G[], float *OBJ, long int *METHOD, long int *IPRINT,
            long int *NDV, long int *NCON, float XL[], float XU[], int mflag,
            char LEB);
