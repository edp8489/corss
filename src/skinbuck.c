#include "corsstol.h"

float Ks(float Z)
{
    return( 4.94002871 + 2.83295655E-1*Z
        - 8.48571232E-3*Z*Z + 2.96028833E-4*Z*Z*Z
        - 5.59394768E-6*pow(Z,4) + 5.12790432E-8*pow(Z,5)
        - 1.79230370E-10*pow(Z,6) );
}


float Kc100(float Z)
{
float y = Z-4.;
if (Z<40) return( -.000179569*y*y*y + .0154754*y*y + .0089413*y + 4.);
    else return(.4*Z);
}

float Kc300(float Z)
{
float y = Z-4.;
if (Z<40) return( -.000159922*y*y*y + .0140199*y*y + .00810034*y + 4.);
else return(.375*Z);
}


float Kc500(float Z)
{
float y = Z-4.;
if (Z<40) return( -.000127178*y*y*y + .011594*y*y + .00669873*y + 4.);
else return(Z/3.);
}


float Kc1000(float Z)
{
float y = Z-4.;
if (Z<40) return( -.000100982*y*y*y + .00965326*y*y + .00557741*y + 4.);
else return(.3*Z);
}


float Kc1500(float Z)
{
float y = Z-4.;
if (Z<40) return( -6.1688E-5*y*y*y + .00674219*y*y + .00389547*y + 4.);
else return(.25*Z);
}


float Kc(float Z, float r, float t)
{
float M, B;
if (Z<4.) return (4.);
if (r/t<100)
{
    M = 0.;
    B = Kc100(Z);
}
else if (r/t < 300)
{
    M = (Kc300(Z) - Kc100(Z) )/200;
    B = Kc100(Z) - M*100;
}
else if (r/t < 500)
{
    M = (Kc500(Z) - Kc300(Z) )/200;
    B = Kc300(Z) - M*300;
}
else if (r/t < 1000)
{
    M = (Kc1000(Z) - Kc500(Z) )/500;
    B = Kc500(Z) - M*500;
}
else if (r/t < 1500)
{
    M = (Kc1500(Z) - Kc1000(Z) )/500;
    B = Kc1000(Z) - M*1000;
}
else
{
    M = 0.;
    B = Kc1500(Z);
}
return(M*(r/t) + B );
} /**************************** end Kc ****/



float skinbuck(struct stringer S, float d, struct material M,
            struct load L, struct cylinder C, float Faxialp, float Io, FILE *out,
            float *Scrpl, float *Sskbd, int SSP, float *Stsk, float *tau,
            float *tau0, float *taucr, float *tauskbd, float Ysk[], float Yst[])
{
    int i;
    float alpha,bpl,g0,g0max, K,K1,Kcr,Ksh,qmax, q[272],Ssk[272],Z;
    if (S.stype == 'I') bpl = S.b;
    if (S.stype == 'H'){
        if ( (S.l2+2*S.la+S.l1) > (S.b-S.l2-2*S.la-S.l1) ){
            bpl = S.l2+2*S.la+S.l1;
        }
        else{
            bpl = S.b-S.l2-2*S.la-S.l1;
        }
    }

    Z = bpl*bpl/C.r/C.t*sqrt(1-M.nu*M.nu);
    Kcr = Kc(Z,C.r,C.t);
    Ksh = Ks(Z);
    if (Ksh < 5.4) Ksh = 5.4;
    if (Kcr < 4) Kcr = 4.;
    if (4==SSP){
        if (C.r/C.t<100.)
            fprintf(out,"\n\nWARNING: r/t < 100, Kc based on r/t = 100");
        if (C.r/C.t>3000.)
            fprintf(out,"\n\nWARNING: r/t > 3000., Kc based on r/t = 3000");
        if (Z>20000.)
            fprintf(out,"\n\nWARNING: Z > 20000., Kc is extrapolated");
    }
    *Scrpl = Kcr*pi*pi*M.E/12/(1-M.nu*M.nu)*pow((C.t/bpl),2);
    *taucr = Ksh*pi*pi*M.E/12/(1-M.nu*M.nu)*pow((C.t/bpl),2);
    if (L.Ph>.001){
        alpha = L.Ph*C.r*C.r/M.E/C.t/C.t;
        K = 9*pow(C.t/C.r,.6)*(1+.21*alpha*pow(C.r/C.t,.6))/(1+3*alpha);
        K1 = .16*C.r/C.t*pow(C.t/d,1.3);
        *Scrpl += (K+K1)*M.E*C.t/C.r;
        *taucr += ( 0 ) ;
        if (SSP == 4)
            fprintf(out,"\n\nPressure Stabilization: Alpha = %f, K = %f, K1 = %f\n",alpha,K,K1);
    }
    if (SSP == 4){
        fprintf(out,"\n\nScrpl = %8.1f,    taucr = %8.1f",*Scrpl,*taucr);
        fprintf(out,"\nbpl = %7.4f,    Kc = %8.4f,    Ks = %8.4f",bpl,Kcr,Ksh);
        fprintf(out,",    Z = %7.4f",Z);
    }
    q[0] = -L.V*L.sfp/Io*S.A/2*Yst[0];
    Ssk[0] = L.M*L.sfp*C.r/Io + Faxialp;
    g0max = fabs(Ssk[0])/(*Scrpl) + pow( (fabs(q[0])/C.t)/(*taucr) , 2 ) - 1.;
    *Sskbd = fabs(Ssk[0]);
    *tauskbd = fabs(q[0])/C.t;
    if (SSP == 4)
    {
        fprintf(out,"\n\n   N                q[I]                 Ssk[I]");
        fprintf(out,"                 G[0]");
        fprintf(out,"\n   0 %20.3f %20.3f %20.3f",q[0],Ssk[0],g0max);
    }
    qmax = fabs(q[0]);
    for (i=1; i<(S.N/4+2); i++){
        q[i] = q[i-1] - L.V*L.sfp/Io*S.A*Yst[i];
        Ssk[i] = L.M*L.sfp*Ysk[i]/Io + Faxialp;
        g0 = fabs(Ssk[i])/(*Scrpl) + pow( (fabs(q[i])/C.t)/(*taucr) , 2 ) - 1.;
        if (g0>g0max)
        {
            g0max = g0;
            *Sskbd = fabs(Ssk[i]);
            *tauskbd = fabs(q[i])/C.t;
        }
        if (fabs(q[i])>qmax) qmax = fabs(q[i]);
        if (SSP == 4) fprintf(out,"\n%4i %20.3f %20.3f %20.3f",i,q[i],Ssk[i],g0);
    }
    *tau = qmax/C.t;
    *Stsk = Ssk[0];
    *tau0 = q[0]/C.t;
    if (SSP == 4)
    {
        fprintf(out,"\n\nMax shear stress = %8.1f,   Max skin stress = %8.1f",*tau,*Stsk);
        fprintf(out,"\nSkin Buckling Drivers: Stress = %8.1f,      Shear Stress = %8.1f",*Sskbd,*tauskbd);
    }
return(g0max);
} /********************end skinbuck ****/





