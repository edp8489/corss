#include "corsstol.h"

float VMStress(char loc[], FILE *out, int SSP, float Sx, float Sy, float tauxy)
{
float S1,S2,VMS;
S1 = (Sx+Sy)/2 + sqrt( (Sx-Sy)*(Sx-Sy)/4 + tauxy*tauxy );
S2 = (Sx+Sy)/2 - sqrt( (Sx-Sy)*(Sx-Sy)/4 + tauxy*tauxy );
VMS = sqrt( ( (S1-S2)*(S1-S2) + S2*S2 + S1*S1 )/2 );
if (4 == SSP)
{
    fprintf(out,"\nPoint of %s:",loc);
    fprintf(out,"\nSx = %8.1f,  Sy = %8.1f,  tauxy = %8.1f",Sx,Sy,tauxy);
    fprintf(out,"\nS1 = %8.1f,  S2 = %8.1f,  Von Mises Stress = %8.1f",S1,S2,VMS);
}
return (VMS);
} /******************** end VMstress ****/


float stresscheck(float Anew, float As, float Ns, float Ph, float r, float t,
                float Faxial, FILE *out, float St, float Scol, int SSP, float tau,
                float tau0, float sf)
{
float VMSA,VMSB,VMSC;
putchar('S');
VMSA = VMStress("Max Compression",out,SSP, Scol,-Ph*r/t*sf,tau0);
VMSB = VMStress("Max Shear",out,SSP, Faxial*(2*pi*(r-t/2)*t+As*Ns)/Anew,-Ph*r/t*sf,tau);
VMSC = VMStress("Max Tension",out,SSP, St,-Ph*r/t*sf,tau0);
if (VMSB>VMSA) {
    VMSA = VMSB;
}
if (VMSC>VMSA) {
    VMSA = VMSC;
}
return(VMSA);
} /************** end stresscheck ****/



