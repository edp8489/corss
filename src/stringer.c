#include "corsstol.h"

void stringer(struct stringer *S, struct cylinder C, FILE *out, int SSP)
{
    int i, na;
    float a[5],ad[5],add[5],alpha,d[5],ha, I[5],ix, sumad, sumadd, sumi,ybar,k1,k2,al,D;
    /* float iy, xbar; */
    putchar('S');
    alpha = S->alp*pi/180;
    if (S->stype == 'H'){
        na = 5;
        ha = (S->h-S->t-S->W)/cos(alpha);
        S->la = ha*sin(alpha);
        a[0] = S->l1*S->t;
        a[1] = ha*S->t;
        a[2] = S->l2*S->W;
        a[3] = a[1];
        a[4] = a[0];
        d[0] = S->t/2;
        d[1] = S->h/2;
        d[2] = S->h-S->W/2;
        d[3] = d[1];
        d[4] = d[0];
        I[0] = S->l1*pow(S->t,3)/12;
        I[1] = S->t*ha/12*(ha*ha*cos(alpha)*cos(alpha) + S->t*S->t*sin(alpha)*sin(alpha) );
        I[2] = S->l2*pow(S->W,3)/12;
        I[3] = I[1];
        I[4] = I[0];
        S->J = 4*pow( (S->h+C.t/2-S->W/2)*(S->l2+S->la) , 2.)/( (S->l2+2*S->la)/C.t + 2*ha/S->t + S->l2/S->W);
    }
    if (S->stype == 'I')
    {
        na = 3;
        ha = (S->h-S->l2-S->l1)/cos(alpha);
        a[0] = S->W*S->l1;
        a[1] = S->t*ha;
        a[2] = S->W*S->l2;
        d[0] = S->l1/2;
        d[1] = (S->h-S->l2-S->l1)/2+S->l1;
        d[2] = S->h-S->l2/2;
        I[0] = S->W*pow(S->l1,3)/12;
        I[1] = S->t*ha/12*(ha*ha*cos(alpha)*cos(alpha)+S->t*S->t*sin(alpha)*sin(alpha));
        I[2] = S->W*pow(S->l2,3)/12;
        k1 = S->W*S->l2*S->l2*S->l2* (1./3. - .21 * S->l2 / S->W * (1. - pow( S->l2 / S->W ,4) / 12 ));
        k2 = (S->h-S->l2)*S->t*S->t*S->t*(1./3. - .105*S->t/(S->h-S->l2)*(1. - pow(S->t/(S->h-S->l2),4)/192));
        if (S->l2<S->t){
            al = S->l2/S->t*.15;
        }
        else{
            al = S->t/S->l2*.15;
        }
        if (S->t>(2*S->l2)){
            D=S->t;
        }
        else{
            D = S->l2 + S->t*S->t/4./S->l2;
        }
        S->J = k1+k2+al*D*D*D*D;
    }
    S->A = 0;
    sumad = 0;
    sumadd = 0;
    sumi = 0;
    for (i=0; i<na; i++){
        ad[i] = a[i]*d[i];
        add[i] = ad[i]*d[i];
        S->A += a[i];
        sumad += ad[i];
        sumadd += add[i];
        sumi += I[i];
    }
    ybar = sumad/(S->A);
    ix = sumi + sumadd - ybar*sumad;
    if (SSP == 4){
        stringerP(na,a,d,ad,add, I,S->A,ybar, ix,"\nA = %f, ybar = %f, Ix = %f",out);
    }
    S->I = ix;
    /* S->J = ix+iy; */
    S->Z = ybar+C.t/2.;
} /************************* end stringer ***/


void stringerP(int na, float a[], float d[], float ad[], float add[],
        float I[], float A, float bar, float ia, char line[],FILE *out)
{
    int i;
    fprintf(out,"\n\n");
    for (i=0;i<na;i++) fprintf(out,"Area %i        ",i); fprintf(out,"\n");
    for (i=0;i<na;i++) fprintf(out,"%f      ",a[i]); fprintf(out,"Area\n");
    for (i=0;i<na;i++) fprintf(out,"%f      ",d[i]); fprintf(out,"d\n");
    for (i=0;i<na;i++) fprintf(out,"%f      ",ad[i]); fprintf(out,"A*d\n");
    for (i=0;i<na;i++) fprintf(out,"%f      ",add[i]); fprintf(out,"A*d*d\n");
    for (i=0;i<na;i++) fprintf(out,"%f      ",I[i]); fprintf(out,"I");
    fprintf(out,line,A,bar, ia);

} /************************* end stringerP ****/
